const pdfMake = require('pdfmake/build/pdfmake');
const fonts = require('pdfmake/build/vfs_fonts');
pdfMake.vfs = fonts.pdfMake.vfs;
// var fonts = {
//     Roboto: {
//         normal: 'fonts/Roboto-Regular.ttf',
//         bold: 'fonts/Roboto-Medium.ttf',
//         italics: 'fonts/Roboto-Italic.ttf',
//         bolditalics: 'fonts/Roboto-MediumItalic.ttf'
//     }
// };

let documentDefinition = {
    styles: {
        header: {
            fontSize: 18,
            bold: true,
            margin: [0, 0, 0, 10]
        },
        table: {
            margin: [0, 5, 0, 15]
        }
    },
    content: [
        { text: 'This is heading', style: 'header' },
        {
            style: 'table',
            table: {
                widths: [100, '*', 200, '*'],
                body: [
                    ['width=100', 'star-sized', 'width=200', 'star-sized'],
                    ['This is test pdf generated from server', { text: 'This is second column', italics: true, color: 'gray' }, { text: 'This is third Column', italics: true, color: 'gray' }, { text: 'This is forth column', italics: true, color: 'gray' }]
                ]
            }
        },
    ]
};


module.exports.pdfDoc = ()=> pdfMake.createPdf(documentDefinition);