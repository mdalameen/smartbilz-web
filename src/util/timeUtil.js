module.exports.getIndiantime = ()=>{
    const destOffset = 5.5 * 60 * 60 * 1000;
    let time = new Date();
    const serverTimeOffset = (time.getTimezoneOffset() * (60 * 1000))
    time.setTime(time.getTime() + destOffset + serverTimeOffset);
    return time;
}