function _getArrayQueryValue(array){
    let s = "";
    for(var i=0; i<array.length; i++){
        s = s + array[i];
        if(i != array.length - 1)
            s = s + ", ";
    }
    return s;
}

function _isValidArray(array){
    console.log(`array is ${array}`);
    let result = false;
    if(array && (array.length > 0))
        result = true;
    return result;
}

module.exports.getArrayStringValue = _getArrayQueryValue;
module.exports.isValidArray = _isValidArray;