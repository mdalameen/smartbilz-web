module.exports.getDbName = (company_name, prefix) => {
    for (value = prefix + '', count = 0, i = 0; i < company_name.length; i++) {
        let ch = company_name.charAt(i);
        if (ch != ' ') {
            value = value + ch;
            count++
        }
        if (count == 5)
            break;
    }
    // let result = await createDb.create(db_name);
    let randomChars = Math.random().toString(36).substr(2, 5);
    value = value + '_' + randomChars;
    return value;
}