module.exports.getFormattedString = (number, size) => {
    var s = `${number}`;
    while (s.length < (size || 2)) {s = "0" + s;}
    return s;
}

module.exports.getFormattedInteger = (string) => parseInt(string, 10);
