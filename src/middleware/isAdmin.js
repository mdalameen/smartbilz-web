const jwt = require('jsonwebtoken');
const config = require('config');

module.exports = (req, res, next)=>{
    let isAdmin = req.user.isAdmin;
    console.log(`isAdmin is ${isAdmin}`);
    if(!isAdmin)
        return res.status(403).send('Your are not allowed to do this operation');
    next();
}