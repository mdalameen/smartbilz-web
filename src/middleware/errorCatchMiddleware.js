const logger = require('../startup/logger');

module.exports = (err, req, res, next)=>{
    logger.error(err.message, err);
    console.log(err);
    res.status(500).send('Internal Server Error');
}