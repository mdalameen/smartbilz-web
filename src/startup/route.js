const express = require('express'),
    auth = require('../middleware/auth');
config = require('config');
require('pug')

const home = require('../routes/home');
const clients = require('../routes/clients');
const contacts = require('../routes/contacts');
const items = require('../routes/items');
const sale_invoices = require('../routes/saleInvoices');
const receipts = require('../routes/receipts');
const purchase_invoices = require('../routes/purchaseInvoices');
const payments = require('../routes/payments');
const dashboards = require('../routes/dashboards');
const tax = require('../routes/tax');
const errorCatcher = require('../middleware/errorCatchMiddleware');


module.exports = (app) => {
    app.set('view engine', 'pug');
    app.use(express.json());
    app.use('/', home);
    app.use('/upload/', express.static(config.get('uploadDirectory')))
    app.use('/api/client/', clients);
    app.use('/api/contact/', auth, contacts);
    app.use('/api/item/', auth, items);
    app.use('/api/invoice/', auth, sale_invoices);
    app.use('/api/receipt/', auth, receipts);
    app.use('/api/purchase/', auth, purchase_invoices);
    app.use('/api/payment/', auth, payments);
    app.use('/api/dashboard/', auth, dashboards);
    app.use('/api/tax/', auth, tax);
    app.use(errorCatcher);
}