const winston = require('winston'),
    config = require('config');
require('winston-daily-rotate-file');

let fileTransport = new (winston.transports.DailyRotateFile)({
    filename: `${config.get('logDirectory')}app-%DATE%.log`,
    datePattern: 'YYYY-MM-DD',
    format:winston.format.combine(
        winston.format.timestamp({format: 'YYYY-MM-DD HH:mm:ss'}),
        winston.format.json() 
    ),
    maxSize: '20m',
    maxFiles: '14d'
  });

let consoleTransport = new (winston.transports.Console)({
    json:false,
    handleExceptions:true,
    colorize:true
});

// winston.exitOnError = true;

let logger = winston.createLogger();
logger.add(fileTransport);
logger.info('check');

if(process.env.NODE_ENV != 'production'){
    logger.add(consoleTransport);
    console.log('logging to console activated');
}

process.on('uncaughtException', (ex)=>{
    console.log(ex);
    logger.error(ex.message, ex);
    process.exit(1);
});

process.on('unhandledRejection', (ex)=>{
    console.log(ex);
    logger.error(ex.message, ex); 
    process.exit(1);
});

module.exports = logger;