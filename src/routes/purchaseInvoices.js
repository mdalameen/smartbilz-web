const express = require('express');
const Joi = require('joi');

const DynamicDb = require('../db/DynamicDb');
const Route = express.Router();
const PurchaseInvoice = require('../db/model/PurchaseInvoice');
const PurchaseStockItem = require('../db/model/PurchaseStockItem');
const PuInvoiceItem = require('../db/model/PuInvoiceItem');
const Sequence = require('../db/Sequence');
const decimalUtil = require('../util/decimalUtil');
const Tax = require('../db/model/Tax')

Route.get('/init', async (req, res) => {
    const db = _getDb(req);
    await new PurchaseInvoice(db).getSchema().sync({ force: true });
    await new PuInvoiceItem(db).getSchema().sync({ force: true });
    await new PurchaseStockItem(db).getSchema().sync({ force: true });
    res.send('Tables Created for this account');
    db.close();
});

Route.get('/nextNumber', async (req, res) => {
    const db = _getDb(req);
    let mSeq = new Sequence(db);
    let invNumber = await mSeq.getNextPurchaseInvoiceNumber();
    res.send(`${decimalUtil.getFormattedString(invNumber, 6)}`);
    db.close();
});

Route.post('/add', async (req, res) => {
    const vErr = _validateInvoice(req.body);
    if (vErr)
        return res.status(400).send(vErr);
    const db = _getDb(req);
    const mPuInvoice = new PurchaseInvoice(db);
    // const mInvoice = new Invoice(db);

    // let vNumber = await mPuInvoice.validateNumber(req.body.pu_invoice_number, undefined);
    // if (vNumber) {
    //     db.close();
    //     return res.status(400).send("Invoice number already exists");
    // }

    let vCustomer = await mPuInvoice.validateVendor(req.body.contact_id);
    if (vCustomer) {
        db.close();
        return res.status(400).send("Please select valid vendor");
    }
    let mTax = new Tax(db);
    let itemList = [];
    for (invoiceItem of req.body.pu_invoice_items) {
        itemList.push(invoiceItem.tax_id);
    }
    console.log(itemList);
    let taxList = await mTax.getTaxByIds(itemList);

    let vAmount = mPuInvoice.validateAmount(req.body, taxList);
    if (vAmount) {
        db.close();
        return res.status(400).send(vAmount);
    }

    // let mSeq = new Sequence(db);
    // await mSeq.updatePurchaseInvoiceNumber(req.body.pu_invoice_number);

    let pu_invoice_items = req.body.pu_invoice_items;
    delete req.body.pu_invoice_items;

    const { pu_invoice_id } = await mPuInvoice.add(req.body);
    console.log(pu_invoice_id);

    const mPuInvoiceItem = new PuInvoiceItem(db);
    // const mInvoiceItem = new InvoiceItem(db);
    await mPuInvoiceItem.addAllItems(pu_invoice_id, pu_invoice_items);

    const out = await mPuInvoice.getPuInvoiceById(pu_invoice_id)

    db.close();
    res.send(out);
});

Route.post('/update/:id', async (req, res) => {

    let id = parseInt(req.params.id, 10);
    if (!id)
        return res.status(400).send('Invalid Id');

    delete req.body.pu_invoice_id;

    const vErr = _validateInvoice(req.body);
    if (vErr)
        return res.status(400).send(vErr);

    const db = _getDb(req);
    // const mInvoice = new Invoice(db);
    const mPuInvoice = new PurchaseInvoice(db);

    let tempInvoice = await mPuInvoice.getPuInvoiceById(id);
    if (!tempInvoice) {
        console.log('insie condition');
        db.close();
        return res.status(404).send("Invoice Not Found");
    }

    // let vNumber = await mPuInvoice.validateNumber(req.body.pu_invoice_number, id);
    // if (vNumber) {
    //     db.close();
    //     return res.status(400).send("Invoice number already exists");
    // }

    let vCustomer = await mPuInvoice.validateVendor(req.body.contact_id);
    if (vCustomer) {
        db.close();
        return res.status(400).send("Invalid customer");
    }

    let mTax = new Tax(db);
    let itemList = [];
    for (invoiceItem of req.body.pu_invoice_items) {
        itemList.push(invoiceItem.tax_id);
    }
    console.log(itemList);
    let taxList = await mTax.getTaxByIds(itemList);

    let vAmount = mPuInvoice.validateAmount(req.body, taxList);
    if (vAmount) {
        db.close();
        return res.status(400).send(vAmount);
    }
    console.log('Validation Successfull!!!');

    // let mSeq = new Sequence(db);
    // await mSeq.updatePurchaseInvoiceNumber(req.body.invoice_number);

    let pu_invoice_items = req.body.pu_invoice_items;
    delete req.body.pu_invoice_items;

    console.log('>>>>>>>>>body');
    console.log(req.body);
    console.log('>>>>>>>>>invoice_id');
    console.log(id);
    const uRes = await mPuInvoice.updateInvoice(id, req.body);
    console.log(uRes);

    const mPuInvoiceItem = new PuInvoiceItem(db);
    // const mInvoiceItem = new InvoiceItem(db);
    await mPuInvoiceItem.updateInvoiceItems(id, pu_invoice_items);
    const out = await mPuInvoice.getPuInvoiceById(id)
    res.send(out);
});


Route.delete('/delete/:id', async (req, res) => {
    let id = parseInt(req.params.id, 10);
    if (!id)
        return res.status(400).send('Invalid Id');

    delete req.body.pu_invoice_id;

    const db = _getDb(req);
    const mPuInvoice = new PurchaseInvoice(db);
    const vResult = await mPuInvoice.validateDelete(id);

    if (vResult) {
        db.close();
        return res.status(400).send(vResult);
    }

    let result = await mPuInvoice.getPuInvoiceById(id);
    if (result) {
        await mPuInvoice.deleteInvoiceById(id);
        db.close();
        return res.send(result);
    } else {
        db.close();
        return res.status(404).send("invoice Not Found");
    }
});

Route.get('/get/:id', async (req, res) => {
    let id = parseInt(req.params.id, 10);
    if (!id)
        return res.status(400).send('Invalid Id');

    const db = _getDb(req);
    const mPuInvoice = new PurchaseInvoice(db);
    // const mInvoice = new Invoice(db);
    const out = await mPuInvoice.getPuInvoiceById(id)
    if (out)
        res.send(out);
    else
        res.status(404).send("Invoice not found");
    db.close();
});

Route.get('/list', async (req, res) => {
    const db = _getDb(req);
    const mPuInvoice = new PurchaseInvoice(db);
    // const mInvoice = new Invoice(db);
    let offset = req.query.offset || 0;
    let limit = req.query.limit || 100;
    result = await mPuInvoice.list(offset, limit);
    res.send(result);
    db.close();
});

function _validateInvoice(invoice) {
    let itemSchema = {
        pu_invoice_item_id: Joi.number(),
        pu_invoice_id: Joi.number(),
        item_id: Joi.number().required(),
        item_name: Joi.string().max(100).required(),
        tax_id: Joi.number().allow(null),
        tax_desc: Joi.string().allow(null),
        item_desc: Joi.string().allow(null),
        is_goods: Joi.boolean().required(),
        igst: Joi.number().allow(null),
        cgst: Joi.number().allow(null),
        sgst: Joi.number().allow(null),
        quantity: Joi.number().required(),
        price: Joi.number().required(),
        discount_amount: Joi.number().required(),
        sub_total: Joi.number().allow(null),
        tax_amount: Joi.number().allow(null),
        unit_id: Joi.number().allow(null),
        is_discount_type_percent: Joi.boolean().allow(null)
    };

    let schema = {
        pu_invoice_number: Joi.string().max(100).required(),
        contact_id: Joi.number().required(),
        pu_invoice_date: Joi.date().required(),
        terms: Joi.number().allow(null),
        due_date: Joi.date().required(),
        order_number: Joi.string().max(100).allow(null),
        notes: Joi.string().allow(null),
        pu_invoice_amount: Joi.number().required(),
        payment_status: Joi.string().valid(['PAID', 'UNPAID']).required(),
        pu_invoice_status: Joi.string().valid(['DRAFT', 'LIVE']).required(),
        pu_invoice_items: Joi.array().items(itemSchema).min(1).required()
    };

    return _validate(invoice, schema);
}

function _validate(text, schema) {
    const { error } = Joi.validate(text, schema);
    if (error)
        return error.details[0].message;
}

let _getDb = (req) => new DynamicDb(req.user.clientName);

module.exports = Route;