const express = require('express');
const Joi = require('joi');

const DynamicDb = require('../db/DynamicDb');
const Tax = require('../db/model/Tax');

const route = express.Router();

route.get('/list', async (req, res) => {
    const db = _getDb(req);
    const mTax = new Tax(db);
    let v = await mTax.list();
    res.send(v);
    db.close();
});

route.post('/add', async (req, res) => {
    const db = _getDb(req);
    let input = req.body;
    delete input.item_id;
    const error = validate(input);
    if (error)
        return res.status(400).send(error);
    const mTax = new Tax(db);
    res.send(await mTax.addTax(input));
    db.close();
});

route.post('/update/:id', async (req, res) => {
    let id = parseInt(req.params.id, 10);
    let input = req.body;
    const db = _getDb(req);
    delete input.item_id;
    const error = validate(input);
    if (error)
        return res.status(400).send(error);
    const mTax = new Tax(db);
    res.send(await mTax.update(input, id));
    db.close();
});

route.delete('/delete/:id', async (req, res) => {
    let id = parseInt(req.params.id, 10);
    const db = _getDb(req);
    const mTax = new Tax(db);
    console.log(await mTax.deleteTax(id));
    res.send('');
    db.close();
});

function _getDb(req) {
    return new DynamicDb(req.user.clientName);
}

function validate(text) {
    const schema = {
        name: Joi.string().max(100).required(),
        percentage: Joi.number()
    };
    const { error } = Joi.validate(text, schema);
    if (error)
        return error.details[0].message;
}


module.exports = route;