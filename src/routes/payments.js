const express = require('express');
const Joi = require('joi');
const DynamicDb = require('../db/DynamicDb');
const Route = express.Router();
const Payment = require('../db/model/Payment');
const PaymentItem = require('../db/model/PaymentItem');
const Sequence = require('../db/Sequence');
const decimalUtil = require('../util/decimalUtil');
const Contact = require('../db/model/Contact');


Route.get('/init', async (req, res) => {
    const db = _getDb(req);
    await new Payment(db).getSchema().sync({ force: true });
    await new PaymentItem(db).getSchema().sync({ force: true });
    res.send('Tables Created for this account');
    db.close();
});


Route.get('/list', async (req, res) => {
    const db = _getDb(req);
    const mPayment = new Payment(db);
    let offset = req.query.offset || 0;
    let limit = req.query.limit || 100;
    result = await mPayment.list(offset, limit);
    res.send(result);
    db.close();
});

Route.get('/nextNumber', async (req, res) => {
    const db = _getDb(req);
    let mSeq = new Sequence(db);
    let invNumber = await mSeq.getNextPaymentNumber();
    res.send(`${decimalUtil.getFormattedString(invNumber, 6)}`);
    db.close();
});

Route.get('/get/:id', async (req, res) => {
    let id = parseInt(req.params.id, 10);
    if (!id)
        return res.status(400).send('Invalid Id');

    const db = _getDb(req);
    const mPayment = new Payment(db);

    let out = await mPayment.getPayment(id);
    if (out)
        res.send(out);
    else
        res.status(404).send("Payment not found");
    db.close();
});

Route.post('/add', async (req, res) => {
    const errMsg = _validatePayment(req.body);
    if (errMsg)
        return res.status(400).send(errMsg);

    const db = _getDb(req);
    const mPayment = new Payment(db);

    const vNumber = await mPayment.validateNumber(req.body.payment_number, undefined);
    if (vNumber) {
        db.close();
        return res.status(400).send('Payment Number Already Exists');
    }

    const vCustomer = await mPayment.validateVendor(req.body.contact_id);
    if (vCustomer) {
        db.close();
        return res.status(400).send('Invalid Vendor Selected');
    }

    const vAmountValidation = mPayment.validateAmount(req.body);

    if (vAmountValidation) {
        db.close();
        return res.status(400).send(vAmountValidation);
    }


    const pendingAmount = await mPayment.getPendingAmount(req.body, undefined);
    console.log('pendingAmount');
    console.log(pendingAmount);
    if (pendingAmount) {
        const vPendingAmount = mPayment.validatePendingAmount(req.body, pendingAmount);
        if (vPendingAmount) {
            db.close();
            return res.status(400).send(vPendingAmount);
        }
    }

    console.log('validation completed!!!');

    let mSeq = new Sequence(db);
    await mSeq.updatePaymentNumber(req.body.payment_number);

    console.log(req.body);
    let payment_items = req.body.payment_items;
    console.log(payment_items);
    delete req.body.payment_items;
    let nPayment = await mPayment.add(req.body);
    const mPaymentItem = new PaymentItem(db);
    await mPaymentItem.addBulk(nPayment.payment_id, payment_items);
    if (pendingAmount)
        await mPayment.updatePuInvoiceStatus(pendingAmount, payment_items, false);

    let out = await mPayment.getPayment(nPayment.payment_id);
    res.send(out);
    db.close();
});

Route.post('/update/:id', async (req, res) => {
    let id = parseInt(req.params.id, 10);
    if (!id)
        return res.status(400).send('Invalid Id');
    delete req.body.payment_id;
    const errMsg = _validatePayment(req.body);
    if (errMsg)
        return res.status(400).send(errMsg);

    const db = _getDb(req);
    const mPayment = new Payment(db);
    let oldPayment = await mPayment.getPayment(id);

    if (!oldPayment) {
        db.close();
        return res.status(404).send("Payment not found");
    }

    const vNumber = await mPayment.validateNumber(req.body.payment_number, id);
    if (vNumber) {
        db.close();
        return res.status(400).send('Payment Number Already Exists');
    }

    const vCustomer = await mPayment.validateVendor(req.body.contact_id);
    if (vCustomer) {
        db.close();
        return res.status(400).send('Invalid Vendor Selected');
    }

    const vAmountValidation = mPayment.validateAmount(req.body);

    if (vAmountValidation) {
        db.close();
        return res.status(400).send(vAmountValidation);
    }

    const pendingAmount = await mPayment.getPendingAmount(req.body, id);
    console.log('pendingAmount');
    console.log(pendingAmount);
    if (pendingAmount) {
        console.log('validated pending amount');
        const vPendingAmount = mPayment.validatePendingAmount(req.body, pendingAmount);
        if (vPendingAmount) {
            db.close();
            return res.status(400).send(vPendingAmount);
        }
    }

    console.log('validation completed!!!');

    let mSeq = new Sequence(db);
    await mSeq.updatePaymentNumber(req.body.payment_number);

    console.log(req.body);
    let payment_items = req.body.payment_items;
    console.log(payment_items);
    delete req.body.payment_items;

    await mPayment.update(id, req.body);
    const mPaymentItem = new PaymentItem(db);
    let updateResult = await mPaymentItem.updatePaymentItems(id, payment_items);
    console.log('updateResult');
    console.log(updateResult);


    if (pendingAmount)
        await mPayment.updatePuInvoiceStatus(pendingAmount, payment_items, false);

    let out = await mPayment.getPayment(id);
    res.send(out);
    db.close();
    console.log('completed');
});

Route.get('/pendingInvoicesForContact/:contact_id', async (req, res) => {
    let contact_id = parseInt(req.params.contact_id, 10);
    if (!contact_id)
        return res.status(400).send('Invalid contact Id');

    const db = _getDb(req);
    const mPayment = new Payment(db);

    let contact = new Contact(db);
    let cResult = await contact.getContact(contact_id);
    if (!cResult) {
        res.status(404).send("Contact not found");
        db.close();
    }

    let list = await mPayment.getPurchaseInvoiceListForContact(contact_id);
    res.send(list);
    db.close();
});


Route.delete('/delete/:id', async (req, res) => {
    let id = parseInt(req.params.id, 10);
    if (!id)
        return res.status(400).send('Invalid Id');

    const db = _getDb(req);
    const mPayment = new Payment(db);
    let payment = await mPayment.getPayment(id);

    if (payment) {
        await mPayment.delete(payment.payment_id);
        let mPaymentItem = new PaymentItem(db);
        await mPaymentItem.deleteItemsByPaymentId(payment.payment_id);
        let pendingAmount = await mPayment.getPendingAmount(payment, undefined);
        console.log('pendingAmount');
        console.log(pendingAmount);
        await mPayment.updatePuInvoiceStatus(pendingAmount, payment.payment_items, true);
        res.send(payment);
    } else
        res.status(404).send("Payment not found");
    db.close();
});

function _validatePayment(payment) {
    let itemSchema = {
        payment_item_id: Joi.number(),
        pu_invoice_id: Joi.number().required(),
        payment_id: Joi.number(),
        amount: Joi.number().required()
    };

    let schema = {
        payment_number: Joi.string().max(100).required(),
        contact_id: Joi.number().required(),
        payment_date: Joi.date().required(),
        payment_amount: Joi.number().required(),
        payment_mode: Joi.string().valid('CASH', 'CARD', 'CHEQUE', 'BANK_TRANSFER').required(),
        reference_number: Joi.string().max(100).allow('').required(),
        unused_payment_amount: Joi.number().required(),
        payment_items: Joi.array().items(itemSchema).required()
    };

    return _validate(payment, schema);
}

function _validate(text, schema) {
    const { error } = Joi.validate(text, schema);
    if (error)
        return error.details[0].message;
}

let _getDb = (req) => new DynamicDb(req.user.clientName);

module.exports = Route;