const express = require('express');
const Joi = require('joi');
const DynamicDb = require('../db/DynamicDb');
const Route = express.Router();
const Receipt = require('../db/model/Receipt');
const ReceiptItem = require('../db/model/ReceiptItem');
const Sequence = require('../db/Sequence');
const decimalUtil = require('../util/decimalUtil');
const Contact = require('../db/model/Contact');


Route.get('/init', async (req, res) => {
    const db = _getDb(req);
    await new Receipt(db).getSchema().sync({ force: true });
    await new ReceiptItem(db).getSchema().sync({ force: true });
    res.send('Tables Created for this account');
    db.close();
});


Route.get('/list', async (req, res) => {
    const db = _getDb(req);
    const mReceipt = new Receipt(db);
    let offset = req.query.offset || 0;
    let limit = req.query.limit || 100;
    result = await mReceipt.list(offset, limit);
    res.send(result);
    db.close();
});

Route.get('/nextNumber', async (req, res) => {
    const db = _getDb(req);
    let mSeq = new Sequence(db);
    let invNumber = await mSeq.getNextReceiptNumber();
    res.send(`${decimalUtil.getFormattedString(invNumber, 6)}`);
    db.close();
});

Route.get('/get/:id', async (req, res) => {
    let id = parseInt(req.params.id, 10);
    if (!id)
        return res.status(400).send('Invalid Id');

    const db = _getDb(req);
    let mReceipt = new Receipt(db);
    let out = await mReceipt.getReceipt(id);
    if (out)
        res.send(out);
    else
        res.status(404).send("Receipt not found");
    db.close();
});

Route.post('/add', async (req, res) => {
    const errMsg = _validateReceipt(req.body);
    if (errMsg)
        return res.status(400).send(errMsg);

    const db = _getDb(req);
    const mReceipt = new Receipt(db);

    if (req.body.receipt_number == null) {
        let mSeq = new Sequence(db);
        let invNumber = await mSeq.getNextReceiptNumber();
        req.body.receipt_number = `${decimalUtil.getFormattedString(invNumber, 6)}`;
    }

    const vNumber = await mReceipt.validateNumber(req.body.receipt_number, undefined);
    if (vNumber) {
        db.close();
        return res.status(400).send('Receipt Number Already Exists');
    }

    const vCustomer = await mReceipt.validateCustomer(req.body.contact_id);
    if (vCustomer) {
        db.close();
        return res.status(400).send('Invalid Customer Selected');
    }

    const vAmountValidation = mReceipt.validateAmount(req.body);

    if (vAmountValidation) {
        db.close();
        return res.status(400).send(vAmountValidation);
    }


    const pendingAmount = await mReceipt.getPendingAmount(req.body, undefined);
    console.log('pendingAmount');
    console.log(pendingAmount);
    if (pendingAmount) {
        const vPendingAmount = mReceipt.validatePendingAmount(req.body, pendingAmount);
        if (vPendingAmount) {
            db.close();
            return res.status(400).send(vPendingAmount);
        }
    }

    let mSeq = new Sequence(db);
    await mSeq.updateReceiptNumber(req.body.receipt_number);

    console.log(req.body);
    let receipt_items = req.body.receipt_items;
    console.log(receipt_items);
    delete req.body.receipt_items;
    let nReceipt = await mReceipt.add(req.body);
    const mReceiptItem = new ReceiptItem(db);
    await mReceiptItem.addBulk(nReceipt.receipt_id, receipt_items);
    if (pendingAmount)
        await mReceipt.updateInvoiceStatus(pendingAmount, receipt_items, false);

    let out = await mReceipt.getReceipt(nReceipt.receipt_id);
    res.send(out);
    db.close();
});

Route.post('/update/:id', async (req, res) => {
    let id = parseInt(req.params.id, 10);
    if (!id)
        return res.status(400).send('Invalid Id');
    delete req.body.receipt_id;
    const errMsg = _validateReceipt(req.body);
    if (errMsg)
        return res.status(400).send(errMsg);

    const db = _getDb(req);
    const mReceipt = new Receipt(db);
    let oldReceipt = await mReceipt.getReceipt(id);
    if (!oldReceipt) {
        db.close();
        return res.status(404).send("Receipt not found");

    }

    const vNumber = await mReceipt.validateNumber(req.body.receipt_number, id);
    if (vNumber) {
        db.close();
        return res.status(400).send('Receipt Number Already Exists');
    }

    const vCustomer = await mReceipt.validateCustomer(req.body.contact_id);
    if (vCustomer) {
        db.close();
        return res.status(400).send('Invalid Customer Selected');
    }

    const vAmountValidation = mReceipt.validateAmount(req.body);

    if (vAmountValidation) {
        db.close();
        return res.status(400).send(vAmountValidation);
    }

    const pendingAmount = await mReceipt.getPendingAmount(req.body, id);
    console.log('pendingAmount');
    console.log(pendingAmount);
    if (pendingAmount) {
        console.log('validated pending amount');
        const vPendingAmount = mReceipt.validatePendingAmount(req.body, pendingAmount);
        if (vPendingAmount) {
            db.close();
            return res.status(400).send(vPendingAmount);
        }
    }

    let mSeq = new Sequence(db);
    await mSeq.updateReceiptNumber(req.body.receipt_number);

    console.log(req.body);
    let receipt_items = req.body.receipt_items;
    console.log(receipt_items);
    delete req.body.receipt_items;

    await mReceipt.update(id, req.body);
    let mReceiptItem = new ReceiptItem(db);
    let updateResult = await mReceiptItem.updateReceiptItems(id, receipt_items);
    console.log('updateResult');
    console.log(updateResult);


    if (pendingAmount)
        await mReceipt.updateInvoiceStatus(pendingAmount, receipt_items, false);

    let out = await mReceipt.getReceipt(id);
    res.send(out);
    db.close();
    console.log('completed');
});

Route.get('/pendingInvoicesForContact/:contact_id', async (req, res) => {
    let contact_id = parseInt(req.params.contact_id, 10);
    if (!contact_id)
        return res.status(400).send('Invalid contact Id');

    const db = _getDb(req);
    let mReceipt = new Receipt(db);

    let contact = new Contact(db);
    let cResult = await contact.getContact(contact_id);
    if (!cResult) {
        res.status(404).send("Contact not found");
        db.close();
    }

    let list = await mReceipt.getInvoiceListForContact(contact_id);
    res.send(list);
    db.close();

});


Route.delete('/delete/:id', async (req, res) => {
    let id = parseInt(req.params.id, 10);
    if (!id)
        return res.status(400).send('Invalid Id');

    const db = _getDb(req);
    let mReceipt = new Receipt(db);
    let receipt = await mReceipt.getReceipt(id);

    if (receipt) {
        await mReceipt.delete(receipt.receipt_id);
        let mReceiptItem = new ReceiptItem(db);
        await mReceiptItem.deleteItemsByReceiptId(receipt.receipt_id);
        let pendingAmount = await mReceipt.getPendingAmount(receipt, undefined);
        console.log('pendingAmount');
        console.log(pendingAmount);
        await mReceipt.updateInvoiceStatus(pendingAmount, receipt.receipt_items, true);
        res.send(receipt);
    } else
        res.status(404).send("Receipt not found");
    db.close();
});

function _validateReceipt(receipt) {
    let itemSchema = {
        receipt_item_id: Joi.number(),
        invoice_id: Joi.number().required(),
        invoice_number: Joi.string().allow(null),
        invoice_date: Joi.date().required(),
        receipt_id: Joi.number(),
        amount: Joi.number().required()
    };

    let schema = {
        receipt_number: Joi.string().max(100).allow(null).required(),
        contact_id: Joi.number().required(),
        receipt_date: Joi.date().required(),
        receipt_amount: Joi.number().required(),
        payment_mode: Joi.string().valid('CASH', 'CARD', 'CHEQUE', 'BANK_TRANSFER').required(),
        reference_number: Joi.string().max(100).allow('').required(),
        unused_receipt_amount: Joi.number().required(),
        receipt_items: Joi.array().items(itemSchema).required()
    };

    return _validate(receipt, schema);
}

function _validate(text, schema) {
    const { error } = Joi.validate(text, schema);
    if (error)
        return error.details[0].message;
}

let _getDb = (req) => new DynamicDb(req.user.clientName);

module.exports = Route;