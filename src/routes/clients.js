const Joi = require('joi');
const bcrypt = require('bcrypt');
const express = require('express');
const config = require('config');
const jwt = require('jsonwebtoken');
const auth = require('../middleware/auth');

const Client = require('../db/model/Client');

const createDb = require('../db/createDb');
const timeUtil = require('../util/timeUtil');
const DynamicDb = require('../db/DynamicDb');
const initClientTable = require('../db/initClientTable');
const Company = require('../db/model/Company');
const Tax = require('../db/model/Tax');
const Unit = require('../db/model/Unit');
const InitialData = require('../db/data/InitialData');

const route = express.Router();

route.post('/login', async (req, res) => {
    const errorMessage = validateLogin(req.body);
    if (errorMessage)
        return res.status(400).send(errorMessage);

    const c = new Client();
    const user = await c.getClientByEmail(req.body.email);
    if (!user)
        return res.status(400).send("Email or Password not valid.");

    const result = await bcrypt.compare(req.body.password, user.password);
    if (!result)
        return res.status(400).send("Email or Password not valid.");

    const token = await getJsonToken(user.client_id, user.db_name, req.body.email);
    let userData = await _getUserData(req.body.email);
    res.header('x-auth-token', token).send(userData);
});


route.post('/register', async (req, res) => {
    const errorMessage = validateRegister(req.body);
    if (errorMessage)
        return res.status(400).send(errorMessage);

    const c = new Client();
    const user = await c.getClientByEmail(req.body.email);
    if (user)
        return res.status(400).send("Email Already Exists.");

    let name = req.body.company_name.toLowerCase();
    const db_name = await createDb.checkAndCreate(name);
    await initClientTable(db_name);

    const hashedPassword = await getHashedPassword(req.body.password);
    const newUser = await c.addClient(req.body.email, hashedPassword, req.body.company_name, db_name, "", req.body.agent_code, timeUtil.getIndiantime());
    const token = getJsonToken(newUser.client_id, newUser.db_name, req.body.email);
    let userData = await _getUserData(req.body.email);
    const db = new DynamicDb(db_name);
    await createInitialData(db);

    db.close();
    res.header('x-auth-token', token).send(userData);
});

route.get('/postLogin', auth, async (req, res) => {
    console.log(req);
    let userData = await _getUserData(req.user.email);
    res.send(userData);
});

async function _getUserData(email) {
    const c = new Client();
    const user = await c.getClientByEmail(email);
    console.log('>>>>>>>>>>>>>>>>>>>>>>>');

    let userData = user.dataValues;
    const db = new DynamicDb(userData.db_name);
    const mCompany = new Company(db);
    userData.company = await mCompany.get();
    console.log(user);
    const mTax = new Tax(db);
    console.log('>>>>>>>>>>>>');
    console.log(userData);
    userData.taxes = await mTax.list();
    console.log(userData);
    db.close();
    console.log('>>>>>>>>>>>>');
    return userData;
}

route.post('/client/forgotPassword', (req, res) => {

});

route.get('/client/subsciption/hasTrial', (req, res) => {

});

route.get('/client/subsciption/trial', (req, res) => {

});

route.post('/client/subsciption/subscribe', (req, res) => {

});

route.get('/client/subsciption/subscriptions', (req, res) => {

});

route.get('/client/subsciption/subscriptions/:id', (req, res) => {

});

route.get('/info/statelist', async (req, res) => {
    res.send(InitialData._getStateCodeList());
});

async function createInitialData(db) {
    var mTax = new Tax(db);
    await mTax.addTaxes(InitialData.getTaxes());
    var mUnit = new Unit(db);
    await mUnit.addUnits(InitialData.getUnits());
}
function validateLogin(requestBody) {
    const schema = {
        email: Joi.string().email().required(),
        password: Joi.string().min(3).max(100).required()
    };
    return _validate(requestBody, schema);
}

function validateRegister(requestBody) {
    const schema = {
        email: Joi.string().email().required(),
        password: Joi.string().min(3).max(100).required(),
        company_name: Joi.string().min(3).required(),
        agent_code: Joi.string().min(3).max(100)
    };
    return _validate(requestBody, schema);
}

function _validate(requestBody, schema) {
    const { error } = Joi.validate(requestBody, schema);
    if (error)
        return error.details[0].message;
}

async function getHashedPassword(password) {
    let salt = await bcrypt.genSalt(10);
    return bcrypt.hash(password, salt);
}

function getJsonToken(client_id, db_name, email) {
    return jwt.sign({ client_id: client_id, clientName: db_name, email: email }, config.get('jwtPrivateKey'));
}

module.exports = route;