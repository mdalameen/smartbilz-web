const express = require('express');
const DynamicDb = require('../db/DynamicDb');
const Route = express.Router();
const DashboardController = require('../db/controller/DashboardController');
const pdfUtil = require('../util/pdfUtil');
const fileUpload = require('express-fileupload');
const multer = require('multer');
const config = require('config');
const Company = require('../db/model/Company');
const Joi = require('joi');


const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, config.get('uploadDirectory'));
    },
    filename: function (req, file, cb) {
        cb(null, req.user.clientName + '_' + file.originalname);
    }
})
const upload = multer({ storage: storage });


Route.get('/get', async (req, res) => {
    const db = _getDb(req);
    const mDash = new DashboardController(db);
    const result = await mDash.getDashboard();
    res.send(result);
    db.close();
});

Route.get('/company/init', async (req, res) => {
    const db = _getDb(req);
    const mCompany = new Company(db);
    await mCompany.init();
    res.send('Company Initialized');
    db.close();
});

Route.get('/company/get', async (req, res) => {
    const db = _getDb(req);
    const mCompany = new Company(db);
    let result = await mCompany.get();
    res.send(result);
    db.close();
});

Route.get('/company/statelist', async (req, res) => {
    res.send(_getStateCodeList());
});

Route.post('/company/update', async (req, res) => {

    const jsonValidation = _validateCompany(req.body);
    if (jsonValidation)
        return res.status(400).send(jsonValidation);
    const db = _getDb(req);
    const mCompany = new Company(db);
    const updateValidate = mCompany.validate(req.body);
    if (updateValidate) {
        db.close();
        return res.status(400).send(updateValidate);
    }
    const updateResult = await mCompany.update(req.body);
    let result = await mCompany.get();
    res.send(result);
    db.close();
});

Route.get('/pdf', async (req, res) => {
    let pdfDoc = pdfUtil.pdfDoc();
    pdfDoc.getBase64((data) => {
        res.writeHead(200, {
            'Content-Type': 'application/pdf'
        });
        const download = Buffer.from(data.toString('utf-8'), 'base64');
        res.end(download);
    });
    // res.send(result);

});

Route.post('/upload', upload.single('image'), async (req, res) => {
    console.log('reached upload');
    console.log(req.file);
    console.log(req.body);

    // console.log(req);
    // req.form.complete
    if (req.files)
        console.log(req.files);
    res.send('reached method');
});

let _getDb = (req) => new DynamicDb(req.user.clientName);

module.exports = Route;

function _validateCompany(json) {

    const schema = {
        place_of_supply_code: Joi.number().allow(null),
        place_of_supply: Joi.string().max(100).allow(null),
        gst_registration_type: Joi.string().valid('TAX_TYPE_UNREGISTERED', 'TAX_TYPE_GST_REGULAR', 'TAX_TYPE_GST_COMPOSITION').required(),
        gstin: Joi.string().max(15).allow(null),
        composition_tax_id: Joi.number().allow(null)
    }

    const { error } = Joi.validate(json, schema);
    if (error)
        return error.details[0].message;
}

