const express = require('express');
const Joi = require('joi');

const DynamicDb = require('../db/DynamicDb');
const Route = express.Router();
const Invoice = require('../db/model/SaleInvoice');
const InvoiceItem = require('../db/model/InvoiceItem');
const Sequence = require('../db/Sequence');
const decimalUtil = require('../util/decimalUtil');
const Tax = require('../db/model/Tax')

Route.get('/init', async (req, res) => {
    const db = _getDb(req);
    await new Invoice(db).getSchema().sync({ force: true });
    await new InvoiceItem(db).getSchema().sync({ force: true });
    res.send('Tables Created for this account');
    db.close();
});

Route.get('/nextNumber', async (req, res) => {
    const db = _getDb(req);
    let mSeq = new Sequence(db);
    let invNumber = await mSeq.getNextSaleInvoiceNumber();
    res.send(`${decimalUtil.getFormattedString(invNumber, 6)}`);
    db.close();
});

Route.post('/add', async (req, res) => {
    delete req.body.invoice_id;
    const vErr = _validateInvoice(req.body, false);
    if (vErr)
        return res.status(400).send(vErr);
    const db = _getDb(req);

    const mInvoice = new Invoice(db);
    if (req.body.invoice_number == null) {
        let mSeq = new Sequence(db);
        let invNumber = await mSeq.getNextSaleInvoiceNumber();
        req.body.invoice_number = `${decimalUtil.getFormattedString(invNumber, 6)}`;
    }

    let vNumber = await mInvoice.validateNumber(req.body.invoice_number, undefined);
    if (vNumber) {
        db.close();
        return res.status(400).send("Invoice number already exists");
    }

    let vCustomer = await mInvoice.validateCustomer(req.body.contact_id);
    if (vCustomer) {
        db.close();
        return res.status(400).send("Please select valid customer");
    }
    let mTax = new Tax(db);
    let itemList = [];
    for (invoiceItem of req.body.invoice_items) {
        itemList.push(invoiceItem.tax_id);
    }
    console.log(itemList);
    let taxList = await mTax.getTaxByIds(itemList);

    let vAmount = mInvoice.validateAmount(req.body, taxList);
    if (vAmount) {
        db.close();
        return res.status(400).send(vAmount);
    }

    let mSeq = new Sequence(db);
    await mSeq.updateSaleInvoiceNumber(req.body.invoice_number);

    let invoice_items = req.body.invoice_items;
    delete req.body.invoice_items;

    const { invoice_id } = await mInvoice.add(req.body);
    console.log(invoice_id);


    const mInvoiceItem = new InvoiceItem(db);
    await mInvoiceItem.addAllItems(invoice_id, invoice_items);

    const out = await mInvoice.getInvoiceById(invoice_id)

    db.close();
    res.send(out);
});

Route.post('/update/:id', async (req, res) => {

    let id = parseInt(req.params.id, 10);
    if (!id)
        return res.status(400).send('Invalid Id');

    delete req.body.invoice_id;

    const vErr = _validateInvoice(req.body, true);
    if (vErr)
        return res.status(400).send(vErr);

    const db = _getDb(req);
    const mInvoice = new Invoice(db);

    let tempInvoice = await mInvoice.getInvoiceById(id);
    if (!tempInvoice) {
        console.log('insie condition');
        db.close();
        return res.status(404).send("Invoice Not Found");
    }

    if (req.body.invoice_number == null) {
        let mSeq = new Sequence(db);
        let invNumber = await mSeq.getNextSaleInvoiceNumber();
        req.body.invoice_number = `${decimalUtil.getFormattedString(invNumber, 6)}`;
    }

    let vNumber = await mInvoice.validateNumber(req.body.invoice_number, id);
    if (vNumber) {
        db.close();
        return res.status(400).send("Invoice number already exists");
    }

    let vCustomer = await mInvoice.validateCustomer(req.body.contact_id);
    if (vCustomer) {
        db.close();
        return res.status(400).send("Invalid customer");
    }
    let mTax = new Tax(db);
    let itemList = [];
    for (invoiceItem of req.body.invoice_items) {
        itemList.push(invoiceItem.tax_id);
    }
    console.log(itemList);
    let taxList = await mTax.getTaxByIds(itemList);

    let vAmount = mInvoice.validateAmount(req.body, taxList);
    if (vAmount) {
        db.close();
        return res.status(400).send(vAmount);
    }
    console.log('Validation Successfull!!!');

    let mSeq = new Sequence(db);
    await mSeq.updateSaleInvoiceNumber(req.body.invoice_number);

    let invoice_items = req.body.invoice_items;
    delete req.body.invoice_items;

    console.log('>>>>>>>>>body');
    console.log(req.body);
    console.log('>>>>>>>>>invoice_id');
    console.log(id);
    const uRes = await mInvoice.updateInvoice(id, req.body);
    console.log(uRes);
    const mInvoiceItem = new InvoiceItem(db);
    await mInvoiceItem.updateInvoiceItems(id, invoice_items);
    const out = await mInvoice.getInvoiceById(id)
    res.send(out);
});


Route.delete('/delete/:id', async (req, res) => {
    let id = parseInt(req.params.id, 10);
    if (!id)
        return res.status(400).send('Invalid Id');

    delete req.body.invoice_id;

    const db = _getDb(req);
    const mInvoice = new Invoice(db);

    const vResult = await mInvoice.validateDelete(id);
    if (vResult) {
        db.close();
        res.status(400).send(vResult);
    }

    let result = await mInvoice.getInvoiceById(id);
    if (result) {
        await mInvoice.deleteInvoiceById(id);
        db.close();
        return res.send(result);
    } else {
        db.close();
        return res.status(404).send("invoice Not Found");
    }
});

Route.get('/get/:id', async (req, res) => {
    let id = parseInt(req.params.id, 10);
    if (!id)
        return res.status(400).send('Invalid Id');

    const db = _getDb(req);
    const mInvoice = new Invoice(db);
    const out = await mInvoice.getInvoiceById(id)
    if (out)
        res.send(out);
    else
        res.status(404).send("Invoice not found");
    db.close();
});

Route.get('/list', async (req, res) => {
    const db = _getDb(req);
    const mInvoice = new Invoice(db);
    let offset = req.query.offset || 0;
    let limit = req.query.limit || 100;
    result = await mInvoice.list(offset, limit);
    res.send(result);
    db.close();
});

function _validateInvoice(invoice, isUpdate) {
    let itemSchema = {
        invoice_id: Joi.number(),
        item_id: Joi.number().required(),
        item_name: Joi.string().max(100).required(),
        item_desc: Joi.string().max(200).allow(null),
        is_goods: Joi.boolean().required(),
        tax_id: Joi.number().allow(null),
        tax_desc: Joi.string().allow(null),
        igst: Joi.number().allow(null),
        cgst: Joi.number().allow(null),
        sgst: Joi.number().allow(null),
        quantity: Joi.number().required(),
        unit_id: Joi.number().allow(null),
        unit_notation: Joi.string().allow(null),
        price: Joi.number().required(),
        discount_amount: Joi.number().required(),
        sub_total: Joi.number().allow(null),
        tax_amount: Joi.number().allow(null),
        is_discount_type_percent: Joi.boolean().allow(null)
    };
    if (isUpdate) {
        itemSchema.invoice_item_id = Joi.number().required();
    } else {
        itemSchema.invoice_item_id = Joi.number().allow(null);
    }

    let schema = {
        invoice_number: Joi.string().max(100).allow(null),
        contact_id: Joi.number().required(),
        invoice_date: Joi.date().required(),
        terms: Joi.number().allow(null),
        due_date: Joi.date().required(),
        order_number: Joi.string().max(100).allow(null),
        notes: Joi.string().allow(null),
        invoice_amount: Joi.number().required(),
        paid_amount: Joi.number().required(),
        payment_status: Joi.string().valid(['PAID', 'UNPAID']).required(),
        invoice_status: Joi.string().valid(['DRAFT', 'LIVE']).required(),
        is_retail_invoice: Joi.boolean().required(),
        invoice_items: Joi.array().items(itemSchema).min(1).required()
    };

    return _validate(invoice, schema);
}

function _validate(text, schema) {
    const { error } = Joi.validate(text, schema);
    if (error)
        return error.details[0].message;
}

let _getDb = (req) => new DynamicDb(req.user.clientName);

module.exports = Route;