const express = require('express');
const Joi = require('joi');

const DynamicDb = require('../db/DynamicDb');
const Item = require('../db/model/Item');
const Group = require('../db/model/Group');
const Unit = require('../db/model/Unit');
const InitialData = require('../db/data/InitialData');
const route = express.Router();

route.get('/getItemList', async (req, res) => {
    let limit = req.query.limit || 1000;
    let offset = req.query.offset || 0;
    console.log(`limit is ${limit} & offset is ${offset}`);
    const db = _getDb(req);
    let mItem = new Item(db);
    let resultList = await mItem.getItemList(limit, offset);
    db.close();
    res.send(resultList);
});

route.get('/getItem/:id', async (req, res) => {
    let id = parseInt(req.params.id, 10);
    if (!id)
        return res.status(400).send('Invalid Id');

    const db = _getDb(req);
    let mItem = new Item(db);

    let result = await mItem.getItemById(id);
    db.close();
    if (result)
        res.send(result);
    else
        res.status(404).send('Item not found');
});

route.post('/addItem', async (req, res) => {
    const error = _validateItem(req.body);
    if (error)
        return res.status(400).send(error);
    const db = _getDb(req);
    let mItem = new Item(db);

    if (mItem.unit_id != null) {
        let mUnit = new Unit(db);
        let unit = await mUnit.getUnit(mItem.unit_id);
        if (!unit)
            return res.status(400).send('selected unit not found');
    }
    //todo
    let result = await mItem.addItem(req.body);
    db.close();
    res.send(result);
});

route.post('/updateItem/:id', async (req, res) => {
    let intId = parseInt(req.params.id, 10);

    if (!intId)
        return res.status(400).send('Invalid Id');

    delete req.body.item_id;
    const error = _validateItem(req.body);
    if (error)
        return res.status(400).send(error);

    if (mItem.unit_id != null) {
        let mUnit = new Unit(db);
        let unit = await mUnit.getUnit(mItem.unit_id);
        if (!unit)
            return res.status(400).send('selected unit not found');
    }

    const db = _getDb(req);
    let mItem = new Item(db);

    let vItem = await mItem.getItemById(intId);
    if (!vItem) {
        db.close();
        res.status(404).send('Item not found');
    }

    let result = await mItem.updateItem(req.body, intId);
    let itemResult = await mItem.getItemById(intId);
    db.close();
    res.send(itemResult);
});

route.delete('/deleteItem/:id', async (req, res) => {
    let id = parseInt(req.params.id, 10);
    if (!id)
        return res.status(400).send('Invalid Id');

    const db = _getDb(req);
    let mItem = new Item(db);

    let outResult = await mItem.getItemById(id);
    if (!outResult) {
        db.close();
        return res.status(404).send('Item not found');
    }

    let delValidate = await mItem.deleteValidation(id);
    if (delValidate) {
        db.close();
        return res.status(400).send(delValidate);
    }

    let result = await mItem.deleteItem(id);
    db.close();
    if (result)
        res.send(outResult);
    else
        res.status(404).send("Item Not Found!");

});

route.get('/getGroups', async (req, res) => {
    const db = _getDb(req);
    let mGroup = new Group(db);
    let resultList = await mGroup.getGroups();
    db.close();
    res.send(resultList);
});

route.post('/addGroup', async (req, res) => {
    const error = _validateGroup(req.body);
    if (error)
        return res.status(400).send(error);
    const db = _getDb(req);
    let mGroup = new Group(db);

    const errorMessage = await mGroup.validate(req.body, undefined);
    if (errorMessage) {
        db.close();
        return res.status(400).send(errorMessage);
    }

    const items = req.body.items;
    delete req.body.items;

    let result = await mGroup.addGroup(req.body);
    console.log(result);
    let group_id = result.group_id;

    await mGroup.addGroupItems(items, group_id);
    let gResult = await mGroup.getGroupById(group_id);
    db.close();
    res.send(gResult);
});

route.post('/updateGroup/:id', async (req, res) => {
    let id = parseInt(req.params.id, 10);
    if (!id)
        return res.status(400).send('Invalid Id');

    delete req.body.group_id;
    const error = _validateGroup(req.body);
    if (error)
        return res.status(400).send(error);

    const db = _getDb(req);
    let mGroup = new Group(db);

    let oldGroupResult = await mGroup.getGroupById(id);
    if (!oldGroupResult) {
        db.close();
        return res.status(404).send("Group Not found");
    }

    const vResult = await mGroup.validate(req.body, id);
    if (vResult) {
        db.close();
        return res.status(400).send(vResult);
    }

    const items = req.body.items;
    delete req.body.items;

    await mGroup.updateGroupItems(items, oldGroupResult.items, id);

    let updatedGroup = await mGroup.getGroupById(id);
    db.close();
    res.send(updatedGroup);
});

route.delete('/deleteGroup/:id', async (req, res) => {
    let id = parseInt(req.params.id, 10);
    if (!id)
        return res.status(400).send('Invalid Id');

    const db = _getDb(req);
    let mGroup = new Group(db);

    let outResult = await mGroup.getGroupById(id);
    if (!outResult) {
        db.close();
        return res.status(404).send("Group Not found");
    }
    let result = await mGroup.deleteGroup(id);
    db.close();
    res.send(outResult);
});

route.get('/getGroup/:id', async (req, res) => {
    let id = parseInt(req.params.id, 10);
    if (!id)
        return res.status(400).send('Invalid Id');

    const db = _getDb(req);
    let mGroup = new Group(db);
    let result = await mGroup.getGroupById(id);
    db.close();
    if (result)
        res.send(result);
    else
        res.status(404).send("Item Group Not Found!");
});

route.get('/unit/init', async (req, res) => {

    const db = _getDb(req);
    let mUnit = new Unit(db);
    await mUnit.getSchema().sync({ force: true });
    let result = await mUnit.addUnits(InitialData.getUnits());
    db.close;
    res.send(result.toString() + ' is result');
});

route.get('/unit/list', async (req, res) => {
    const db = _getDb(req);
    let mUnit = new Unit(db);
    let result = await mUnit.getUnits();
    db.close;
    res.send(result);
});

route.post('/unit/add', async (req, res) => {
    const error = _validateUnit(req.body);
    if (error)
        return res.status(400).send(error);
    if (req.body.unit_id)
        delete req.body.unit_id;
    const db = _getDb(req);
    let mUnit = new Unit(db);
    if (req.body.parent_id != null) {
        let unit = await mUnit.getParentUnit(req.body.parent_id);
        if (unit == null || unit.parent_id != null) {
            db.close();
            return res.status(400).send('Parent should not be a child');
        }
    } else
        req.body.conversion_rate = 1.0;
    let unit = await mUnit.addUnit(req.body);
    res.send(unit);
});

function _getDb(req) {
    return new DynamicDb(req.user.clientName);
}


function _validateGroup(bodyText) {
    let items = {
        group_item_id: Joi.number(),
        item_id: Joi.number(),
        item_name: Joi.string().allow(null),
        sales_price: Joi.number().allow(null)
    };

    let schema = {
        group_name: Joi.string().max(100).required(),
        group_discount_percent: Joi.number().required(),
        items: Joi.array().items(items)
    };

    return _validate(bodyText, schema);
}

function _validateUnit(bodyText) {
    const schema = {
        unit_id: Joi.number().allow(null),
        unit_name: Joi.string().max(100).required(),
        unit_notation: Joi.string().max(100).required(),
        parent_id: Joi.number().allow(null),
        conversion_rate: Joi.number().required()
    };
    return _validate(bodyText, schema);
}

function _validateItem(bodyText) {
    const schema = {
        item_name: Joi.string().max(100).required(),
        item_type: Joi.string().valid('GOODS', 'SERVICE').required(),
        unit_id: Joi.number().allow(null).required(),
        // unit_notation: Joi.string().max(100).allow(null).required(),
        sku: Joi.string().max(100).allow(null),
        item_code: Joi.string().max(100).allow(null),
        hsnsac: Joi.string().max(100).allow(null),
        mrp_price: Joi.number().allow(null),
        sales_price: Joi.number().allow(null),
        purchase_price: Joi.number().allow(null),
        is_taxable: Joi.boolean(),
        tax_id: Joi.number().allow(null),
        cess_percent: Joi.number().allow(null),
        is_inventory: Joi.boolean().allow(null),
        has_expiry_date: Joi.boolean().allow(null),
        notify_expiry: Joi.number().allow(null),
        opening_stock: Joi.number().allow(null),
        opening_stock_expiry_date: Joi.date().allow(null)
    };
    return _validate(bodyText, schema);
}

function _validate(text, schema) {
    const { error } = Joi.validate(text, schema);
    if (error)
        return error.details[0].message;
}



module.exports = route;