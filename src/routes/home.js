const express = require('express');
const router = express.Router();
const dateAndTime = require('date-and-time');
const {getIndiantime} = require('../util/timeUtil');
const dateFormat = 'DD/MM/YYYY hh:mm A';


getTimeString = ()=>{
    let dateString = dateAndTime.format(getIndiantime(), dateFormat);
    return dateString.slice(0, 6) + dateString.slice(8, dateString.length);
}


router.get('/', (req, res)=>{
    res.render('home', {message:`time is: ${getTimeString()}`});
});


module.exports = router;