
const express = require('express');
const Joi = require('joi');

const DynamicDb = require('../db/DynamicDb');
const Contact = require('../db/model/Contact');

const route = express.Router();

route.get('/list', async (req, res) => {
    let c = _getContact(req);
    let limit = req.query.limit || 1000;
    let offset = req.query.offset || 0;
    let type = req.query.type;

    let resultList = await c.getAllContactsMeta(offset, limit, type);
    res.send(resultList);
});

route.post('/addContact', async (req, res) => {
    const errMsg = _validateContact(req.body);
    if (errMsg)
        return res.status(400).send(errMsg);
    let contact = _getContact(req);
    let result = await contact.addContact(req.body.contact_name, req.body.display_name, req.body.type, req.body.email, req.body.telephone, req.body.mobile, req.body.website, req.body.gst_type, req.body.gstin, req.body.place_of_supply, req.body.place_of_supply_code, req.body.payment_terms, req.body.address_line_1, req.body.address_line_2, req.body.city, req.body.state, req.body.pincode);
    res.send(result);
});

route.get('/getContact/:id', async (req, res) => {
    let id = parseInt(req.params.id, 10);
    if (!id)
        return res.status(400).send('Invalid Id');

    let contact = _getContact(req);
    let result = await contact.getContact(req.params.id);
    if (result)
        res.send(result);
    else
        res.status(404).send("No Contact Found");
});

route.post('/updateContact/:id', async (req, res) => {
    let id = parseInt(req.params.id, 10);
    if (!id)
        return res.status(400).send('Invalid Id');

    const errMsg = _validateContact(req.body);
    if (errMsg)
        return res.status(400).send(errMsg);
    let contact = _getContact(req);
    let result = await contact.updateContacts(req.params.id, req.body.contact_name, req.body.display_name, req.body.type, req.body.email, req.body.telephone, req.body.mobile, req.body.website, req.body.gst_type, req.body.gstin, req.body.place_of_supply, req.body.place_of_supply_code, req.body.payment_terms, req.body.address_line_1, req.body.address_line_2, req.body.city, req.body.state, req.body.pincode);
    let contactResult = await contact.getContact(req.params.id);
    res.send(contactResult);
});

module.exports = route;

function _getContact(req) {
    let db = new DynamicDb(req.user.clientName);
    return new Contact(db);
}
function _validateContact(bodyJson) {
    let schema = {
        contact_name: Joi.string().min(3).max(200).required(),
        display_name: Joi.string().min(3).max(200).allow(null),
        type: Joi.string().valid('CUSTOMER', 'VENDOR').required(),
        email: Joi.string().email().allow(null),
        telephone: Joi.string().max(20).allow(null),
        mobile: Joi.string().max(20).allow(null),
        website: Joi.string().max(200).allow(null),
        gst_type: Joi.string().valid('TAX_TYPE_GST_REGULAR', 'TAX_TYPE_GST_COMPOSITION', 'TAX_TYPE_UNREGISTERED', 'TAX_TYPE_CONSUMER').allow(null),
        gstin: Joi.string().min(15).max(15).alphanum().allow(null),
        place_of_supply: Joi.string().allow(null),
        place_of_supply_code: Joi.number(),
        payment_terms: Joi.number(),
        address_line_1: Joi.string().max(100).allow(null),
        address_line_2: Joi.string().max(100).allow(null),
        city: Joi.string().max(100).allow(null),
        state: Joi.string().max(100).allow(null),
        pincode: Joi.string().max(10).allow(null)
    };
    const { error } = Joi.validate(bodyJson, schema);
    if (error)
        return error.details[0].message;
}