const sequalize = require('sequelize');
const connection = require('./connection');
const clientUtil = require('../util/clientUtil');
const config = require('config');

function create(db_name) {
    return connection.query(`create database ${db_name}`, { type: sequalize.QueryTypes.INSERT });
}

module.exports.create = create;

module.exports.checkAndCreate = async (company_name) => {
    console.log('inside )CreateDB function');
    const prefix = config.get('clientDbPrefix');
    let willCreateDb = true;
    let db_name = '';
    while (willCreateDb) {
        db_name = clientUtil.getDbName(company_name, prefix);
        try {
            const res = await create(db_name);
            willCreateDb = false;
        } catch (err) {
            console.log(err);
        }
    }

    return db_name;
}