const Sequelize = require('sequelize');
const dat = require('date-and-time');
const dashboardQuery = require('../query/dashboardQuery');
const timeUtil = require('../../util/timeUtil');


class DashboardController {
    constructor(dynamicDb) {
        this.db = dynamicDb;
    }

    async getDashboard() {
        let output = {};
        const totRec = await this.db.getConnection().query(dashboardQuery.getPayableAndReceivable(timeUtil.getIndiantime()), { type: Sequelize.QueryTypes.SELECT });
        console.log('totRec');
        console.log(totRec);
        output.total_receivable = totRec[0].total_receivable;
        output.total_payable = totRec[0].total_payable;
        output.overdue_receivable = totRec[0].overdue_receivable;
        output.overdue_payable = totRec[0].overdue_payable;

        let fromDate = timeUtil.getIndiantime();
        fromDate.setDate(1);
        fromDate.setMonth(fromDate.getMonth() - 5);
        console.log('fromDate');
        console.log(fromDate);
        let toDate = timeUtil.getIndiantime();
        console.log('toDate');
        console.log(toDate);

        const prevCashflow = await this.db.getConnection().query(dashboardQuery.prevCashFlow(fromDate), { type: Sequelize.QueryTypes.SELECT });
        console.log('prevCashflow');
        console.log(prevCashflow);
        let currentCash = prevCashflow[0].prevCashFlow;

        const cashflow = await this.db.getConnection().query(dashboardQuery.cashflow(fromDate, toDate), { type: Sequelize.QueryTypes.SELECT });

        let currentMonth = fromDate.getMonth() + 1;
        let fullCashFlow = [];
        for (let i = 0; i < 6; i++) {
            let cashItem = cashflow.find(c => c.month == currentMonth);

            if (!cashItem)
                cashItem = { month: 0, cash_flow: 0 }

            cashItem.month = this._getAlphabetMonth(currentMonth);
            cashItem.cash_flow = currentCash = cashItem.cash_flow + currentCash;
            fullCashFlow.push(cashItem);
            currentMonth = (currentMonth) % 12 + 1;
        }
        output.cash_flows = fullCashFlow;

        const incExp = await this.db.getConnection().query(dashboardQuery.incomeExpense(fromDate, toDate), { type: Sequelize.QueryTypes.SELECT });
        console.log('incExp');
        console.log(incExp);
        currentMonth = fromDate.getMonth() + 1;
        let fullIncExp = [];
        for (let i = 0; i < 6; i++) {
            let ie = incExp.find(c => c.month == currentMonth);
            if (!ie)
                ie = { month: 0, income: 0, expense: 0 };
            ie.month = this._getAlphabetMonth(currentMonth);
            fullIncExp.push(ie);
            currentMonth = (currentMonth) % 12 + 1;
        }

        output.income_and_expenses = fullIncExp;
        return output;
    }

    _reverseArray(array) {
        let a = []
        for (let i = array.length - 1; i >= 0; i--)
            a.push(array[i]);
        return a;
    }

    _getAlphabetMonth(month) {
        // console.log('inputMonth');
        console.log(month);
        switch (month) {
            case 1:
                return "JAN";
            case 2:
                return "FEB";
            case 3:
                return "MAR";
            case 4:
                return "APR";
            case 5:
                return "MAY";
            case 6:
                return "JUN";
            case 7:
                return "JUL";
            case 8:
                return "AUG";
            case 9:
                return "SEP";
            case 10:
                return "OCT";
            case 11:
                return "NOV";
            case 12:
                return "DEC";
        }
    }
}

module.exports = DashboardController;