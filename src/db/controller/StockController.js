const Item = require('../model/Item');
const Sequelize = require('sequelize');
const InStock = require('../model/InStock');

const OutStock = require('../model/OutStock');

// const Product = require('../model/Item');
const InvoiceStockItem = require('../model/InvoiceStockItem');
const PurchaseStockItem = require('../model/PurchaseStockItem');
const OpeningStock = require('../model/OpeningStock');
const Unit = require('../model/Unit');


class StockController {
    constructor(dynamicDb) {
        this.db = dynamicDb;
    }

    addInStock(stock) {
        const mInStock = new InStock(this.db);
        return mInStock.add(stock);
    }

    addInStocks(stocks) {
        const mInStock = new InStock(this.db);
        return mInStock.addBulk(stocks);
    }

    updateInStock(stock, in_stock_id) {
        delete stock.in_stock_id;
        const mInStock = new InStock(this.db);
        return mInStock.update(stock, in_stock_id);
    }

    deleteInStock(in_stock_id) {
        const mInStock = new InStock(this.db);
        return mInStock.delete(in_stock_id);
    }

    addOutStock(stock) {
        const mOutStock = new OutStock(this.db);
        return mOutStock.add(stock);
    }

    addOutStocks(stocks) {
        const mOutStock = new OutStock(this.db);
        return mOutStock.addBulk(stocks);
    }

    updateOutStock(stock, out_stock_id) {
        const mOutStock = new OutStock(this.db);
        delete stock.out_stock_id;
        return mOutStock.update(stock, out_stock_id);
    }

    deleteOutStock(out_stock_id) {
        const mOutStock = new OutStock(this.db);
        return mOutStock.delete(out_stock_id);
    }

    async addOpeningStock(item_id, quantity, expiry_date) {
        let stock = { item_id: item_id, quantity: quantity };
        if (expiry_date)
            stock.expiry_date = expiry_date;
        const addStockResult = await this.addInStock(stock);
        console.log('addStockResult');
        console.log(addStockResult);
        const mOpenStock = new OpeningStock(this.db);
        const openResult = await mOpenStock.add(item_id, addStockResult.dataValues.in_stock_id);
        console.log('openResult');
        console.log(openResult);
    }

    async deleteOpeningStock(item_id) {
        const mOpenStock = new OpeningStock(this.db);
        const stock = await mOpenStock.get(item_id);
        await mOpenStock.deleteByItemId(item_id);
        const mInStock = new InStock(this.db);
        if (stock)
            await mInStock.delete(stock.in_stock_id);
    }

    async updateOpeningStock(in_stock_id, quantity, expiry_date) {
        const mInStock = new InStock(this.db);
        await mInStock.update({ in_stock_id: in_stock_id, quantity: quantity, expiry_date: expiry_date }, in_stock_id);
    }

    async addInvoiceItems(items, mItem) {
        // const tItem = new Product();
        console.log('items');
        console.log(items);
        let lineItemIdSet = new Set();
        for (let item of items)
            lineItemIdSet.add(item.item_id);

        console.log("line item id set");
        console.log(lineItemIdSet);
        console.log("before Creating item");
        // const mItem = new Item(this.db);
        // console.log("after Creating item");
        const mapItems = await mItem.getSelectedItemsMap(Array.from(lineItemIdSet));
        console.log('mapItems');
        console.log(mapItems);

        let mInvoiceStock = new InvoiceStockItem(this.db);
        for (let addedItem of items) {
            console.log('mapItems.get(addedItem.item_id)');
            console.log(mapItems.get(addedItem.item_id));
            if (mapItems.get(addedItem.item_id).is_inventory) {
                let mUnit = new Unit(this.db);
                let unit = await mUnit.getUnit(addedItem.unit_id);
                if (unit != null) {
                    let quantity = unit.conversion_rate * addedItem.quantity;
                    let outStockResult = await this.addOutStock({ item_id: addedItem.item_id, quantity: quantity });
                    let out_stock_id = outStockResult.dataValues.out_stock_id;
                    await mInvoiceStock.add(addedItem.invoice_item_id, out_stock_id);
                    console.log(outStockResult);
                }
            }
        }
        console.log("stock added successfully");
    }

    async updateSalesItems(items, mItem) {
        console.log('items');
        console.log(items);

    }

    async addPuInvoiceItems(items, mItem) {
        //start doing
        console.log('items');
        console.log(items);
        let lineItemIdSet = new Set();
        for (let item of items)
            lineItemIdSet.add(item.item_id);

        console.log("line item id set");
        console.log(lineItemIdSet);
        console.log("before Creating item");
        // const mItem = new Item(this.db);
        console.log("after Creating item");
        const mapItems = await mItem.getSelectedItemsMap(Array.from(lineItemIdSet));
        console.log('mapItems');
        console.log(mapItems);

        let mPurchaseStockItem = new PurchaseStockItem(this.db);
        // let mInvoiceStock = new InvoiceStockItem(this.db);
        for (let addedItem of items) {
            console.log('mapItems.get(addedItem.item_id)');
            console.log(mapItems.get(addedItem.item_id));
            let item = mapItems.get(addedItem.item_id);
            if (item.is_inventory) {
                let mUnit = new Unit(this.db);
                let unit = await mUnit.getUnit(addedItem.unit_id);
                console.log(unit);
                let quantity = addedItem.quantity * unit.conversion_rate;
                console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
                console.log(quantity);

                let stock = { item_id: addedItem.item_id, quantity: quantity };
                if (item.has_expiry_date)
                    stock.expiry_date = addedItem.expiry_date;

                let inStockResult = await this.addInStock(stock);
                // let outStockResult = await this.addOutStock({item_id:addedItem.item_id, quantity:addedItem.quantity});
                let in_stock_id = inStockResult.dataValues.in_stock_id;
                // let out_stock_id = outStockResult.dataValues.out_stock_id;
                await mPurchaseStockItem.add(addedItem.pu_invoice_item_id, in_stock_id);
                // await mInvoiceStock.add(addedItem.invoice_item_id, out_stock_id);
                console.log('inStockResult');
                console.log(inStockResult);
            }
        }
        console.log("stock added successfully");
    }

    async deleteInvoiceItems(invoice_items, mItem) {
        console.log('-----========------delete invoice items started');
        let itemIdList = [];//to get items to verify is inventory
        let invoiceItemIdList = []; //toget out stock
        let invoiceItemMap = new Map(); //toGet outstockId

        for (let invItem of invoice_items) {
            console.log('invItem');
            console.log(invItem);
            invoiceItemMap.set(invItem.invoice_item_id, invItem);
            itemIdList.push(invItem.item_id);
            invoiceItemIdList.push(invItem.invoice_item_id);
        }
        console.log('invoiceItemMap');
        console.log(invoiceItemMap);
        console.log('itemIdList');
        console.log(itemIdList);

        const mInvoiceStockItem = new InvoiceStockItem(this.db);
        // const mItem = new Item(this.db);
        const mapItems = await mItem.getSelectedItemsMap(itemIdList);
        console.log('mapItems');
        console.log(mapItems);

        const invoice_stock_items = await mInvoiceStockItem.getInvoiceStockItemList(invoiceItemIdList);
        console.log('invoice_stock_items');
        console.log(invoice_stock_items);
        let outStockIds = [];
        let invoiceItemIds = [];

        for (let invStockItem of invoice_stock_items) {
            let invItem = invoiceItemMap.get(invStockItem.invoice_item_id);
            let item = mapItems.get(invItem.item_id);
            if (item.is_inventory) {
                invoiceItemIds.push(invStockItem.invoice_item_id);
                outStockIds.push(invStockItem.out_stock_id);
            }
        }
        console.log('invoiceStockIds');
        console.log(invoiceItemIds);
        await mInvoiceStockItem.deleteByInvoiceItemId(invoiceItemIds);
        const mOutStock = new OutStock(this.db);
        await mOutStock.deleteAll(outStockIds);
        console.log('stock deleted successfully');
    }

    async deletePuInvoiceItems(pu_invoice_items, mItem) {
        console.log('-----========------delete invoice items started');
        let itemIdList = [];//to get items to verify is inventory
        let invoiceItemIdList = []; //
        let invoiceItemMap = new Map(); //to get from array

        for (let invItem of pu_invoice_items) {
            console.log('invItem');
            console.log(invItem);
            invoiceItemMap.set(invItem.pu_invoice_item_id, invItem);
            itemIdList.push(invItem.item_id);
            invoiceItemIdList.push(invItem.pu_invoice_item_id);
        }
        console.log('invoiceItemMap');
        console.log(invoiceItemMap);
        console.log('itemIdList');
        console.log(itemIdList);

        let mPurchaseStockItem = new PurchaseStockItem(this.db);
        // const mInvoiceStockItem = new InvoiceStockItem(this.db);
        // const mItem = new Item(this.db);
        const mapItems = await mItem.getSelectedItemsMap(itemIdList);
        console.log('mapItems');
        console.log(mapItems);

        const invoice_stock_items = await mPurchaseStockItem.getPuInvoiceStockItemList(invoiceItemIdList);
        console.log('invoice_stock_items');
        console.log(invoice_stock_items);
        let inStockIds = [];
        let invoiceItemIds = [];

        for (let invStockItem of invoice_stock_items) {
            let invItem = invoiceItemMap.get(invStockItem.pu_invoice_item_id);
            let item = mapItems.get(invItem.item_id);
            if (item.is_inventory) {
                invoiceItemIds.push(invStockItem.pu_invoice_item_id);
                inStockIds.push(invStockItem.in_stock_id);
            }
        }
        console.log('inStockIds');
        console.log(inStockIds);
        console.log('invoiceStockIds');
        console.log(invoiceItemIds);
        await mPurchaseStockItem.deleteByInvoiceItemId(invoiceItemIds);
        // await mInvoiceStockItem.deleteByInvoiceItemId(invoiceItemIds);
        // const mOutStock = new OutStock(this.db);
        const mInStock = new InStock(this.db);
        await mInStock.deleteAll(inStockIds);
        console.log('stock deleted successfully');
    }
}

module.exports = StockController;