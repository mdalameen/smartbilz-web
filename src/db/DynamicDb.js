const Sequelize = require('sequelize');
config = require('config');

class DynamicDb {

    constructor(db_name) {
        this.db_name = db_name;
        this.connection = new Sequelize(this.db_name, config.get("dbUserName"), config.get('dbPassword'), {
            host: config.get('dbHost'),
            dialect: 'postgres',
            operatorsAliases: false,
            define: {
                timestamps: false
            }
        });
    }

    getConnection() {
        return this.connection;
    }

    close() {
        this.connection.close();
    }

    async isConnected() {
        try {
            let connection = this.getConnection();
            await connection.authenticate();
            console.log(`${this.db_name} id connected.`);
            return true;
        } catch (err) {
            console.log(err);
            new Error(err);
            return false;
        }
    }

    initTables() {
        let connection = this.getConnection();
        console.log(`init tables here`);
        console.log(connection.host);
    }
}

module.exports = DynamicDb;