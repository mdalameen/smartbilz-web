const DynamicDb = require('./DynamicDb');
const logger = require('../startup/logger');

const Contact = require('./model/Contact');
const Item = require('./model/Item');
const Group = require('./model/Group');
const group_item = require('./model/GroupItem');
const SaleInvoice = require('./model/SaleInvoice');
const InvoiceItem = require('./model/InvoiceItem');
const Receipt = require('./model/Receipt');
const ReceiptItem = require('./model/ReceiptItem');
const Sequence = require('./Sequence');
const InStock = require('./model/InStock');
const OutStock = require('./model/OutStock');
const InvoiceStockItem = require('./model/InvoiceStockItem');
const OpeningStock = require('./model/OpeningStock');
const PurchaseInvoice = require('./model/PurchaseInvoice');
const PuInvoiceItem = require('./model/PuInvoiceItem');
const PurchaseStockItem = require('./model/PurchaseStockItem');
const Payment = require('./model/Payment');
const PaymentItem = require('./model/PaymentItem');
const Company = require('./model/Company');
const Tax = require('./model/Tax');
const Unit = require('./model/Unit');



async function initializeClientTable(db_name) {
    let db = new DynamicDb(db_name);
    await createTable(Contact, db);
    await createTable(Item, db);
    await createTable(Group, db);
    await createTable(group_item, db);
    await createTable(SaleInvoice, db);
    await createTable(InvoiceItem, db);
    await createTable(Receipt, db);
    await createTable(ReceiptItem, db);
    await createTable(InStock, db);
    await createTable(OutStock, db);
    await createTable(InvoiceStockItem, db);
    await createTable(OpeningStock, db);
    await createTable(PurchaseInvoice, db);
    await createTable(PuInvoiceItem, db);
    await createTable(PurchaseStockItem, db);
    await createTable(Payment, db);
    await createTable(PaymentItem, db);
    await createTable(Tax, db);
    await createTable(Unit, db);
    const mCompany = new Company(db);
    await mCompany.init();


    try {
        await _initSequences(db);
    } catch (error) {
        console.log('sequence already created');
        console.log(error);
    }

    logger.info(`client tables created for db_name ${db_name}`);
    db.close();
}

function createTable(Model, db) {
    return new Model(db).getSchema().sync({ force: true });
}

function _initSequences(db) {
    return new Sequence(db).init();
}

module.exports = initializeClientTable;