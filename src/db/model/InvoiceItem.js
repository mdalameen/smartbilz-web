const Sequelize = require('sequelize');
const StockController = require('../controller/StockController');
const Item = require('./Item');

class InvoiceItem {
    constructor(dynamicDb) {
        this.db = dynamicDb;
        this.schema = this.db.getConnection().define('sale_invoice_item', {
            invoice_item_id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            }, invoice_id: {
                type: Sequelize.INTEGER,
                allowNull: false
            }, item_id: {
                type: Sequelize.INTEGER,
                allowNull: false
            }, item_name: Sequelize.STRING(100),
            item_desc: Sequelize.STRING(200),
            is_goods: Sequelize.BOOLEAN,
            // tax_percent: Sequelize.DOUBLE,
            tax_id: Sequelize.DOUBLE,
            tax_desc: Sequelize.DOUBLE,
            igst: Sequelize.DOUBLE,
            cgst: Sequelize.DOUBLE,
            sgst: Sequelize.DOUBLE,
            quantity: Sequelize.DOUBLE,
            price: Sequelize.DOUBLE,
            discount_amount: Sequelize.DOUBLE,
            is_discount_type_percent: Sequelize.BOOLEAN,
            sub_total: Sequelize.DOUBLE,
            tax_amount: Sequelize.DOUBLE,
            unit_id: Sequelize.INTEGER,
            unit_notation: Sequelize.STRING(100),
        });
    }

    getSchema() { return this.schema };

    async addAllItems(invoice_id, items) {

        console.log('ToaddedItems');
        console.log(items);

        for (let item of items)
            item.invoice_id = invoice_id;

        let result = await this.schema.bulkCreate(items, { returning: true });

        let addedItems = [];
        for (let resultItem of result)
            addedItems.push(resultItem.dataValues);

        console.log('addedItems');
        console.log(addedItems);

        const mStockController = new StockController(this.db);
        const mItem = new Item(this.db);
        await mStockController.addInvoiceItems(addedItems, mItem);

        console.log('insert result');
        console.log(result);

        return result;
    }

    getList(invoice_id) {
        return this.schema.findAll({ where: { invoice_id: invoice_id }, raw: true });
    }

    updateItem(invoice_item_id, item) {
        return this.schema.update(item, { where: { invoice_item_id: invoice_item_id } });
    }

    async updateInvoiceItems(invoice_id, items) {
        let oldItems = await this.getList(invoice_id);
        let deleteItems = [];
        let addItems = [];
        let updateItems = [];

        let allItemMap = new Map();

        for (var nItem of items) {
            if (nItem.invoice_item_id) {
                allItemMap.set(nItem.invoice_item_id, nItem);
            } else
                addItems.push(nItem);
        }
        console.log(`allItemsInMap`);
        console.log(allItemMap);

        for (var oItem of oldItems) {
            let mapItem = allItemMap.get(oItem.invoice_item_id);
            if (mapItem)
                updateItems.push(mapItem);
            else
                deleteItems.push(oItem);
        }

        const mStockController = new StockController(this.db);

        console.log(`updatedItems arrays`);
        console.log(updateItems);
        console.log(`addItems arrays`);
        console.log(addItems);
        console.log(`deleteItems arrays`);
        console.log(deleteItems);
        if (deleteItems.length > 0) {
            let delResult = await this._deleteItems(deleteItems);
            const mItem = new Item(this.db);
            await mStockController.deleteInvoiceItems(deleteItems, mItem);
            console.log(`=========================\ndeleteResult\n${delResult}`);
        }

        if (addItems.length > 0) {
            let insResult = await this.addAllItems(invoice_id, addItems);
            console.log(`=========================\ninsertResult`);
            console.log(insResult);
        }

        for (var updateItem of updateItems) {
            let invoice_item_id = updateItem.invoice_item_id;
            delete updateItem.invoice_item_id;
            console.log(`updateInvoiceItemIs:${invoice_item_id}`);
            console.log(`updatedInItemIs:${updateItem}`);
            let updateResult = await this.updateItem(invoice_item_id, updateItem);
            console.log(`=========================\nupdateResult\n${updateResult}`);
            updateItem.invoice_item_id = invoice_item_id;
        }
        if (updateItems.length > 0) {
            const mItem = new Item(this.db);
            await mStockController.deleteInvoiceItems(updateItems, mItem);
            await mStockController.addInvoiceItems(updateItems, mItem);
        }
    }


    async _deleteItems(invoice_items) {
        let items = [];
        for (let item of invoice_items)
            items.push(item.invoice_item_id);
        //delete stock and invoiceStock entry
        //only one method should be used for delete
        return this.schema.destroy({ where: { invoice_item_id: { [Sequelize.Op.in]: items } } });
    }

    async deleteByInvoiceId(invoice_id) {
        console.log("delete by invoice_id");
        console.log(invoice_id);
        const items = await this.getList(invoice_id);
        const result = await this.schema.destroy({ where: { invoice_id: invoice_id } })
        console.log('items');
        console.log(items);
        let invoice_items = [];
        for (let invoiceItem of items)
            invoice_items.push(invoiceItem);
        console.log('invoice_items');
        console.log(invoice_items);
        const mStockController = new StockController(this.db);
        const mItem = new Item(this.db);
        await mStockController.deleteInvoiceItems(invoice_items, mItem);
        return result;
    }
}

module.exports = InvoiceItem;