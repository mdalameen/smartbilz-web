const Sequelize = require('sequelize');

class ReceiptItem {
    constructor(DynamicDb) {
        this.db = DynamicDb;
        this.schema = this.db.getConnection().define('receipt_item', {
            receipt_item_id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            invoice_id: Sequelize.INTEGER,
            invoice_number: Sequelize.STRING,
            invoice_date: Sequelize.DATEONLY,
            receipt_id: Sequelize.INTEGER,
            amount: Sequelize.DOUBLE
        });
    }

    getSchema() { return this.schema };

    async addBulk(receipt_id, items) {
        for (let item of items) {
            item.receipt_id = receipt_id;
            delete item.receipt_item_id;
        }
        let result = await this.schema.bulkCreate(items, { raw: true });
        // console.log(result);
        return result;
    }

    deleteItemsByReceiptId(receipt_id) {
        return this.schema.destroy({ where: { receipt_id: receipt_id } });
    }

    getItemsByReceiptId(receipt_id) {
        return this.schema.findAll({ where: { receipt_id: receipt_id }, raw: true });
    }

    getList(receipt_id) {
        return this.schema.findAll({ where: { receipt_id: receipt_id }, raw: true });
    }

    async updateReceiptItems(receipt_id, items) {
        let oldItems = await this.getList(receipt_id);
        let deleteIds = [];
        let addItems = [];
        let updateItems = [];

        let allItemMap = new Map();

        for (var nItem of items) {
            if (nItem.receipt_item_id) {
                allItemMap.set(`${nItem.receipt_item_id}`, nItem);
            } else
                addItems.push(nItem);
        }
        console.log(`allItemsInMap`);
        console.log(allItemMap);

        for (var oItem of oldItems) {
            let newItem = allItemMap.get(`${oItem.receipt_item_id}`);
            if (newItem)
                updateItems.push(newItem);
            else
                deleteIds.push(oItem.receipt_item_id);
        }


        console.log(`addItems${addItems}`);
        console.log(`deleteItems${deleteIds}`);
        if (deleteIds.length > 0) {
            let delResult = await this.deleteByIds(deleteIds);
            console.log(`=========================\ndeleteResult\n${delResult}`);
        }

        if (addItems.length > 0) {
            let insResult = await this.addBulk(receipt_id, addItems);
            console.log(`=========================\ninsertResult\n${insResult}`);
        }

        console.log(`updateItems:${updateItems}`);
        for (var updateItem of updateItems) {
            let receipt_item_id = updateItem.receipt_item_id;
            updateItem.receipt_id = receipt_id;
            delete updateItem.receipt_item_id;
            console.log(`updateInvoiceItemIs:${receipt_item_id}`);
            console.log(`updatedInItemIs:${updateItem}`);
            let updateResult = await this.updateItem(receipt_item_id, updateItem);
            console.log(`=========================\nupdateResult\n${updateResult}`);
        }
        console.log(`update completed`);
    }

    deleteByIds(receipt_item_id) {
        return this.schema.destroy({ where: { receipt_item_id: { [Sequelize.Op.in]: receipt_item_id } } });
    }

    updateItem(receipt_item_id, item) {
        return this.schema.update(item, { where: { receipt_item_id: receipt_item_id } })
    }

}





module.exports = ReceiptItem;