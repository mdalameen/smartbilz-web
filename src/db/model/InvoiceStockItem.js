const Sequelize = require('sequelize');

class InvoiceStockItem {
    constructor(dynamicDb) {
        this.db = dynamicDb;
        this.schema = this.db.getConnection().define('invoice_stock_item', {
            invoice_stock_item_id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            invoice_item_id: Sequelize.INTEGER,
            out_stock_id: Sequelize.INTEGER
        });
    }

    getSchema() {
        return this.schema;
    }

    add(invoice_item_id, out_stock_id) {
        return this.schema.create({ invoice_item_id: invoice_item_id, out_stock_id: out_stock_id });
    }

    async getInvoiceStockItemList(invoiceItemIds) {
        const result = await this.schema.findAll({ where: { invoice_item_id: { [Sequelize.Op.in]: invoiceItemIds } } });
        let invoice_stock_items = [];
        for (let r of result)
            invoice_stock_items.push(r.dataValues);
        return invoice_stock_items;
    }

    deleteByInvoiceItemId(invoiceItemIds) {
        return this.schema.destroy({ where: { invoice_item_id: { [Sequelize.Op.in]: invoiceItemIds } } });
    }
}

module.exports = InvoiceStockItem;