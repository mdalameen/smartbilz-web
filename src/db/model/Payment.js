const Sequelize = require('sequelize');
const Contact = require('./Contact');
const query = require('../query/purchaseQuery');
const PurchaseInvoice = require('./PurchaseInvoice');
const PaymentItem = require('./PaymentItem');

class Payment {
    constructor(DynamicDb) {
        this.db = DynamicDb;
        this.schema = this.db.getConnection().define('payment', {
            payment_id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            }, payment_number: {
                type: Sequelize.STRING(100),
                unique: true,
                allowNull: false
            }, contact_id: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            payment_date: Sequelize.DATEONLY,
            payment_amount: Sequelize.DOUBLE,
            unused_payment_amount: Sequelize.DOUBLE,
            payment_mode: {
                type: Sequelize.ENUM,
                values: ['CASH', 'CARD', 'CHEQUE', 'BANK_TRANSFER'],
                allowNull: false
            }, reference_number: Sequelize.STRING(100)
        });
    }
    async validateNumber(payment_number, payment_id) {
        let output = await this.db.getConnection().query(query.getPaymentNumberCount(payment_number, payment_id), { type: Sequelize.QueryTypes.SELECT });
        let result = false;
        if (output[0].count > 0)
            result = true;
        console.log('output');
        console.log(output);
        console.log(result);
        return result;
    }

    async validateVendor(contact_id) {
        let contact = new Contact(this.db);
        let customer = await contact.getContact(contact_id);
        if (customer)
            return customer.type != 'VENDOR';
        return true;
    }

    async updatePuInvoiceStatus(pendingAmountResult, payment_items, isDelete) {
        console.log(">>>>>>>>>>>>>>>>>>updatingInvoiceStatus");
        console.log(payment_items);
        let invoiceMap = new Map();
        let mPuInvoice = new PurchaseInvoice(this.db);
        for (let p of pendingAmountResult)
            invoiceMap.set(`${p.pu_invoice_id}`, p.pendingAmount);
        console.log('invoiceMap');
        console.log(invoiceMap);

        for (let item of payment_items) {
            console.log(`itemAmount:${item.amount}`);
            console.log(`mapAmount:${invoiceMap.get(`${item.pu_invoice_id}`)}`);
            if (isDelete || item.amount != invoiceMap.get(`${item.pu_invoice_id}`))
                await mPuInvoice.updateStatus(item.pu_invoice_id, "UNPAID");
            else
                await mPuInvoice.updateStatus(item.pu_invoice_id, "PAID");
        }
        console.log('>>>>>>>>>>>>>>status updated<<<<<<<<<<<<<<<');
    }

    async getPayment(payment_id) {
        let rResult = await this.schema.findOne({ where: { payment_id: payment_id }, raw: true });
        if (rResult) {
            let itemResult = await this.getPaymentItem(payment_id);
            console.log(itemResult);
            rResult.payment_items = itemResult;
        }
        return rResult;
    }

    getPaymentItem(payment_id) {
        const mPaymentItem = new PaymentItem(this.db);
        return mPaymentItem.getItemsByPaymentId(payment_id);
    }

    delete(payment_id) {
        return this.schema.destroy({ where: { payment_id: payment_id } });
    }

    async getPendingAmount(payment, payment_id) {
        let puInvoiceIds = [];
        for (let item of payment.payment_items) {
            puInvoiceIds.push(item.pu_invoice_id);
        }

        if (puInvoiceIds.length > 0) {
            let pendingAmountResult = await this.db.getConnection().query(query.getPendingAmount(puInvoiceIds, payment_id), { type: Sequelize.QueryTypes.INSERT });
            console.log('pendingAmountResult');
            console.log(pendingAmountResult);
            return pendingAmountResult[0];
        }
    }

    validatePendingAmount(payment, pendingAmountResult) {
        console.log('pendingAmountResult');
        console.log(pendingAmountResult);
        let invoiceMap = new Map();
        for (let p of pendingAmountResult)
            invoiceMap.set(`${p.pu_invoice_id}`, p.pendingAmount);
        console.log(invoiceMap);

        for (let item of payment.payment_items) {
            let pendingAmount = invoiceMap.get(`${item.pu_invoice_id}`);
            console.log(pendingAmount);
            console.log('pendingAmount');

            if (pendingAmount <= 0)
                return "The invoice have no pending amount";

            if (!pendingAmount)
                continue;

            if (pendingAmount < item.amount)
                return "Receipt amount is greater than pending amount";

            if (item.amount <= 0)
                return "Invalid Amount";
        }
    }

    add(payment) {
        return this.schema.create(payment, { raw: true });
    }

    update(payment_id, payment) {
        return this.schema.update(payment, { where: { payment_id: payment_id } });
    }

    getPurchaseInvoiceListForContact(contact_id) {
        return this.db.getConnection().query(query.getPendingPurchaseInvoiceListForContact(contact_id), { type: Sequelize.QueryTypes.SELECT });
    }


    async list(offset, limit) {
        let cResult = await this.db.getConnection().query(query.getPaymentCount(), { type: Sequelize.QueryTypes.SELECT });
        let count = cResult[0].count;
        let lResult = await this.db.getConnection().query(query.paymentList(offset, limit), { type: Sequelize.QueryTypes.SELECT });
        return { count: count, data: lResult };
    }

    validateAmount(payment) {
        let totalItemAmount = 0;
        let invoiceSet = new Set();

        if (payment.payment_amount <= 0)
            return "Invalid Amount";

        for (let item of payment.payment_items) {
            totalItemAmount += item.amount;
            if (invoiceSet.has(item.pu_invoice_id))
                return "Duplicate invoice entries";
            invoiceSet.add(item.pu_invoice_id);
        }

        totalItemAmount += payment.unused_payment_amount;
        if (totalItemAmount != payment.payment_amount)
            return "Invalid Amount";
    }

    getSchema() { return this.schema };
}

module.exports = Payment;