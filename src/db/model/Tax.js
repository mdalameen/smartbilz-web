const Sequelize = require('sequelize');

class Tax {
    constructor(dynamicDb) {
        this.db = dynamicDb;
        this.schema = this.db.getConnection().define('tax', {
            tax_id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            name: Sequelize.STRING,
            percentage: Sequelize.DOUBLE,
            type: Sequelize.STRING
        });
    }

    async addTaxes(taxes) {
        return this.schema.bulkCreate(taxes);
    }

    async addTax(tax) {
        return this.schema.create(tax);
    }

    async deleteTax(tax_id) {
        return this.schema.destroy({ where: { tax_id: tax_id } });
    }

    async list() {
        return this.schema.findAll();
    }

    async update(tax, tax_id) {
        delete tax.tax_id;
        return this.schema.update(tax, { where: { tax_id: tax_id } });
    }

    getSchema() {
        return this.schema;
    }

    async getTaxById(tax_id) {
        if (!tax_id)
            return;
        let tax = await this.schema.findOne({ where: { tax_id: tax_id }, raw: true });
        console.log(tax);
        return tax;
    }

    async getTaxByIds(tax_ids) {
        const result = await this.schema.findAll({ where: { tax_id: { [Sequelize.Op.in]: tax_ids } }, raw: true });
        console.log(result);
        return result;
    }
}



module.exports = Tax;