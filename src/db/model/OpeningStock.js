const Sequelize = require('sequelize');

class OpeningStock {
    constructor(dynamicDb) {
        this.db = dynamicDb;
        this.schema = this.db.getConnection().define('opening_stock', {
            opening_stock_id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            item_id: Sequelize.INTEGER,
            in_stock_id: Sequelize.INTEGER
        });
    }

    getSchema() { return this.schema };

    get(item_id) {
        return this.schema.findOne({ where: { item_id: item_id } });
    }

    add(item_id, in_stock_id) {
        return this.schema.create({ item_id: item_id, in_stock_id: in_stock_id });
    }

    update(opening_stock_id, item_id, in_stock_id) {
        return this.schema.update({ item_id: item_id, in_stock_id: in_stock_id }, { where: { opening_stock_id: opening_stock_id } });
    }

    deleteByItemId(item_id) {
        return this.schema.destroy({ where: { item_id: item_id } });
    }

}

module.exports = OpeningStock;