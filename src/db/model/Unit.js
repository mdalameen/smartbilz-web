const sequelize = require('sequelize');

class Unit {
    constructor(dynamicDb) {
        this.dynamicDb = dynamicDb;
        this.schema = this.dynamicDb.getConnection().define('unit', {
            unit_id: {
                type: sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            }, unit_name: {
                type: sequelize.STRING(100)
            }, unit_notation: {
                type: sequelize.STRING(100)
            }, parent_id: {
                type: sequelize.INTEGER
            }, conversion_rate: {
                type: sequelize.DOUBLE
            }
        });
    }

    async initSequence() {
        const countResult = await this.db.getConnection().query(query.hasPuInvoice(pu_invoice_id), { type: Sequelize.QueryTypes.SELECT });
        return countResult;
    }

    async getUnits() {
        let result = await this.schema.findAndCountAll();
        let list = [];
        let count = result.count;
        for (let v of result.rows)
            list.push(v.dataValues);
        return { count: count, data: list };
    }

    async getUnit(unit_id) {
        return this.schema.findOne({ where: { unit_id: unit_id } });
    }



    addUnits(units) {
        return this.schema.bulkCreate(units);
    }

    addUnit(unit) {
        return this.schema.create(unit);
    }

    updateUnit(unit, unit_id) {
        return this.schema.update(unit, { where: { unit_id: unit_id } });
    }

    deleteUnit(unit_id) {
        return this.schema.destroy({ where: { unit_id: unit_id } });
    }


    getParentUnit(parent_id) {
        return this.schema.findOne({ where: { unit_id: parent_id } });
    }

    getSchema() {
        return this.schema;
    }


}

module.exports = Unit;