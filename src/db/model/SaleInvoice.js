const Sequelize = require('sequelize');
const query = require('../query/saleQuery');
const Contact = require('./Contact');
const InvoiceItem = require('./InvoiceItem');

class SaleInvoice {
    constructor(dynamicDb) {
        this.db = dynamicDb;
        this.schema = this.db.getConnection().define('sale_invoice', {
            invoice_id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            }, invoice_number: {
                type: Sequelize.STRING(100),
                unique: true,
                allowNull: false
            }, contact_id: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            invoice_date: Sequelize.DATEONLY,
            terms: Sequelize.INTEGER,
            due_date: Sequelize.DATEONLY,
            order_number: Sequelize.STRING(100),
            notes: Sequelize.TEXT,
            invoice_amount: Sequelize.DOUBLE,
            paid_amount: Sequelize.DOUBLE,
            payment_status: {
                type: Sequelize.ENUM,
                values: ['PAID', 'UNPAID'],
                allowNull: false
            },
            invoice_status: {
                type: Sequelize.ENUM,
                values: ['DRAFT', 'LIVE']
            },
            is_retail_invoice: {
                type: Sequelize.BOOLEAN,
                defaultValue: false
            }
        });
    }

    getSchema() { return this.schema; }


    async validateNumber(invoice_number, invoice_id) {
        let output = await this.db.getConnection().query(query.getInvoiceNumberCount(invoice_number, invoice_id), { type: Sequelize.QueryTypes.SELECT });
        let result = false;
        if (output[0].count > 0)
            result = true;
        console.log(output);
        console.log(result);
        return result;
    }

    async validateCustomer(customerId) {
        let contact = new Contact(this.db);
        let customer = await contact.getContact(customerId);
        if (customer)
            return customer.type != 'CUSTOMER';
        return true;
    }

    async add(invoice) {
        let result = await this.schema.create(invoice);
        return result.get({ plane: true });
    }

    async validateHasInvoice(invoice_id) {
        const countResult = await this.db.getConnection().query(query.hasInvoice(invoice_id), { type: Sequelize.QueryTypes.SELECT });
        console.log(countResult[0].count);
        return countResult[0].count > 0;
    }

    updateInvoice(invoice_id, invoice) {
        return this.schema.update(invoice, { where: { invoice_id: invoice_id } });
    }

    async getInvoiceById(invoice_id) {
        // let invList = await this.schema.findOne({ where: { invoice_id: invoice_id }, raw: true });
        // console.log(invList);
        let invList = await this.db.getConnection().query(query.getInvoiceById(invoice_id), { type: Sequelize.QueryTypes.SELECT });

        if (invList.length > 0) {
            let inv = invList[0];
            const invItems = new InvoiceItem(this.db);
            let itemList = await invItems.getList(invoice_id);
            // console.log('>>>>>>>>> itemList');
            // console.log(itemList);
            inv.invoice_items = itemList;
            return inv;
        }
        return null;
    }

    async deleteInvoiceById(invoice_id) {
        await this.schema.destroy({ where: { invoice_id: invoice_id } });
        const invItems = new InvoiceItem(this.db);
        await invItems.deleteByInvoiceId(invoice_id);
    }

    async list(offset, limit) {
        let cResult = await this.db.getConnection().query(query.getInvoiceCount(), { type: Sequelize.QueryTypes.SELECT });
        let count = cResult[0].count;
        let lResult = await this.db.getConnection().query(query.getInvoiceList(offset, limit), { type: Sequelize.QueryTypes.SELECT });
        return { count: count, data: lResult };
    }

    updateStatus(invoice_id, status) {
        if (!(status == 'PAID' || status == 'UNPAID'))
            new Error("Invalid payment status");
        return this.schema.update({ payment_status: status }, { where: { invoice_id: invoice_id } });
    }

    validateAmount(invoice, taxes) {
        console.log('tax is');
        console.log(taxes);
        let invoice_amount = 0;
        for (let item of invoice.invoice_items) {

            let cSubTotal = ((item.quantity * item.price) - item.discount_amount);
            if (cSubTotal != item.sub_total)
                return "Incorrect Item, quantity, price & subtotal value";
            let cgst = item.cgst || 0;
            let igst = item.igst || 0;
            let sgst = item.sgst || 0;
            let tax_amount = item.tax_amount || 0;
            let taxPercentage = 0;
            for (var t of taxes)
                if (t.tax_id == item.tax_id)
                    taxPercentage = t.percentage;
            console.log('taxPercentage');
            console.log(taxPercentage);
            // if (tax)
            //     taxPercentage = (tax.percentage) || 0;

            console.log(`cgst${cgst}`);
            console.log(`igst${igst}`);
            console.log(`sgst${sgst}`);
            console.log(`tax_amount${tax_amount}`);

            if (cgst > 0 && igst > 0 && sgst > 0)
                return "Incorrect gst amount";

            if (cgst != sgst)
                return "Incorrect cgst & sgst";


            console.log((cSubTotal / 100) * taxPercentage);
            console.log(tax_amount);
            if (((cSubTotal / 100) * taxPercentage) != tax_amount)
                return "Incorrect tax percentage";

            if ((cgst + igst + sgst) != tax_amount)
                return "Incorrect tax amount";

            invoice_amount += (cSubTotal + tax_amount);
        }

        if (invoice_amount != invoice.invoice_amount)
            return "Incorrect invoice amount";
    }

    async validateDelete(invoice_id) {
        let result = await this.db.getConnection().query(query.invoiceDeleteValidation(invoice_id), { type: Sequelize.QueryTypes.SELECT });
        if (result[0].count > 0)
            return "Delete Receipt of this invoice first";
    }
}

module.exports = SaleInvoice;