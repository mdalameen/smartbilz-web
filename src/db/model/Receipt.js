const Sequelize = require('sequelize');
const Contact = require('./Contact');
const query = require('../query/saleQuery');
const queryUtil = require('../../util/queryUtil');
const SaleInvoice = require('./SaleInvoice');
const ReceiptItem = require('./ReceiptItem');

class Receipt {
    constructor(DynamicDb) {
        this.db = DynamicDb;
        this.schema = this.db.getConnection().define('receipt', {
            receipt_id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            }, receipt_number: {
                type: Sequelize.STRING(100),
                unique: true,
                allowNull: false
            }, contact_id: {
                type: Sequelize.INTEGER,
                allowNull: false
            }, receipt_date: Sequelize.DATEONLY,
            receipt_amount: Sequelize.DOUBLE,
            unused_receipt_amount: Sequelize.DOUBLE,
            payment_mode: {
                type: Sequelize.ENUM,
                values: ['CASH', 'CARD', 'CHEQUE', 'BANK_TRANSFER'],
                allowNull: false
            }, reference_number: Sequelize.STRING(100)
        });
    }
    async validateNumber(receipt_number, receipt_id) {
        let output = await this.db.getConnection().query(query.getReceiptNumberCount(receipt_number, receipt_id), { type: Sequelize.QueryTypes.SELECT });
        let result = false;
        if (output[0].count > 0)
            result = true;
        console.log(output);
        console.log(result);
        return result;
    }

    async validateCustomer(contact_id) {
        let contact = new Contact(this.db);
        let customer = await contact.getContact(contact_id);
        if (customer)
            return customer.type != 'CUSTOMER';
        return true;
    }

    async updateInvoiceStatus(pendingAmountResult, receipt_items, isDelete) {
        console.log(">>>>>>>>>>>>>>>>>>updatingInvoiceStatus");
        console.log(receipt_items);
        let invoiceMap = new Map();
        let mInvoice = new SaleInvoice(this.db);
        for (let p of pendingAmountResult)
            invoiceMap.set(`${p.invoice_id}`, p.pendingAmount);

        for (let item of receipt_items) {
            console.log(`itemAmount:${item.amount}`);
            console.log(`mapAmount:${invoiceMap.get(`${item.invoice_id}`)}`);
            if (isDelete || item.amount != invoiceMap.get(`${item.invoice_id}`))
                await mInvoice.updateStatus(item.invoice_id, "UNPAID");
            else
                await mInvoice.updateStatus(item.invoice_id, "PAID");
        }
        console.log('>>>>>>>>>>>>>>status updated<<<<<<<<<<<<<<<');
    }

    async getReceipt(receipt_id) {
        // let rResult = await this.schema.findOne({ where: { receipt_id: receipt_id }, raw: true });
        let rReuslt = await this.db.getConnection().query(query.getReceipt(receipt_id), { type: Sequelize.QueryTypes.SELECT });
        if (rReuslt.length > 0) {
            let v = rReuslt[0];
            let itemResult = await this.getReceiptItem(receipt_id);

            console.log(itemResult);
            v.receipt_items = itemResult;
            return v;
        }
        return null;
    }

    getReceiptItem(receipt_id) {
        const mReceiptItem = new ReceiptItem(this.db);
        return mReceiptItem.getItemsByReceiptId(receipt_id);
    }

    delete(receipt_id) {
        return this.schema.destroy({ where: { receipt_id: receipt_id } });
    }

    async getPendingAmount(receipt, receipt_id) {
        let invoiceIds = [];
        for (let item of receipt.receipt_items) {
            invoiceIds.push(item.invoice_id);
        }

        if (invoiceIds.length > 0) {
            let pendingAmountResult = await this.db.getConnection().query(query.getPendingAmount(invoiceIds, receipt_id), { type: Sequelize.QueryTypes.INSERT });
            return pendingAmountResult[0];
        }
    }

    validatePendingAmount(receipt, pendingAmountResult) {
        console.log(pendingAmountResult);
        let invoiceMap = new Map();
        for (let p of pendingAmountResult)
            invoiceMap.set(`${p.invoice_id}`, p.pendingAmount);
        console.log(invoiceMap);

        for (let item of receipt.receipt_items) {
            let pendingAmount = invoiceMap.get(`${item.invoice_id}`);
            console.log(pendingAmount);
            console.log('pendingAmount');

            if (pendingAmount <= 0)
                return "The invoice have no pending amount";

            if (!pendingAmount)
                continue;

            if (pendingAmount < item.amount)
                return "Receipt amount is greater than pending amount";
            if (item.amount <= 0)
                return "Invalid Amount";
        }
    }

    add(receipt) {
        return this.schema.create(receipt, { raw: true });
    }

    update(receipt_id, receipt) {
        return this.schema.update(receipt, { where: { receipt_id: receipt_id } });
    }

    getInvoiceListForContact(contact_id) {
        return this.db.getConnection().query(query.getPendingInvoiceListForContact(contact_id), { type: Sequelize.QueryTypes.SELECT });
    }


    async list(offset, limit) {
        let cResult = await this.db.getConnection().query(query.getReceiptCount(), { type: Sequelize.QueryTypes.SELECT });
        let count = cResult[0].count;
        let lResult = await this.db.getConnection().query(query.receiptList(offset, limit), { type: Sequelize.QueryTypes.SELECT });
        return { count: count, data: lResult };
    }

    validateAmount(receipt) {
        let totalItemAmount = 0;
        let invoiceSet = new Set();

        if (receipt.receipt_amount <= 0)
            return "Invalid Amount";
        for (let item of receipt.receipt_items) {
            totalItemAmount += item.amount;
            if (invoiceSet.has(item.invoice_id))
                return "Duplicate invoice entries";
            invoiceSet.add(item.invoice_id);
        }

        totalItemAmount += receipt.unused_receipt_amount;
        if (totalItemAmount != receipt.receipt_amount)
            return "Invalid Amount";
    }

    getSchema() { return this.schema };
}

module.exports = Receipt;