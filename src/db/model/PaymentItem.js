const Sequelize = require('sequelize');

class PaymentItem {
    constructor(DynamicDb) {
        this.db = DynamicDb;
        this.schema = this.db.getConnection().define('payment_item', {
            payment_item_id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            pu_invoice_id: Sequelize.INTEGER,
            payment_id: Sequelize.INTEGER,
            amount: Sequelize.DOUBLE
        });
    }

    getSchema() { return this.schema };

    async addBulk(payment_id, items) {
        for (let item of items) {
            item.payment_id = payment_id;
            delete item.receipt_item_id;
        }
        let result = await this.schema.bulkCreate(items, { raw: true });
        // console.log(result);
        return result;
    }

    deleteItemsByPaymentId(payment_id) {
        return this.schema.destroy({ where: { payment_id: payment_id } });
    }

    getItemsByPaymentId(payment_id) {
        return this.schema.findAll({ where: { payment_id: payment_id }, raw: true });
    }

    getList(payment_id) {
        return this.schema.findAll({ where: { payment_id: payment_id }, raw: true });
    }

    async updatePaymentItems(payment_id, items) {
        let oldItems = await this.getList(payment_id);
        let deleteIds = [];
        let addItems = [];
        let updateItems = [];

        let allItemMap = new Map();

        for (var nItem of items) {
            if (nItem.payment_item_id) {
                allItemMap.set(`${nItem.payment_item_id}`, nItem);
            } else
                addItems.push(nItem);
        }
        console.log(`allItemsInMap`);
        console.log(allItemMap);
        console.log(`addItems`);
        console.log(addItems);

        for (var oItem of oldItems) {
            let newItem = allItemMap.get(`${oItem.payment_item_id}`);
            if (newItem)
                updateItems.push(newItem);
            else
                deleteIds.push(oItem.payment_item_id);
        }

        console.log(`deleteItems`);
        console.log(deleteIds);

        console.log('updateItems');
        console.log(updateItems);

        if (deleteIds.length > 0) {
            let delResult = await this.deleteByIds(deleteIds);
            console.log(`=========================\ndeleteResult\n${delResult}`);
        }

        if (addItems.length > 0) {
            let insResult = await this.addBulk(payment_id, addItems);
            console.log(`=========================\ninsertResult\n${insResult}`);
        }

        for (var updateItem of updateItems) {
            let payment_item_id = updateItem.payment_item_id;
            updateItem.payment_id = payment_id;
            delete updateItem.payment_item_id;
            console.log('receipt_item_id');
            console.log(payment_item_id);
            console.log('updateItem');
            console.log(updateItem);
            let updateResult = await this.updateItem(payment_item_id, updateItem);
            console.log(`=========================\nupdateResult`);
            console.log(updateResult);

        }
        console.log(`update completed`);
    }

    deleteByIds(paymentItemIds) {
        return this.schema.destroy({ where: { payment_item_id: { [Sequelize.Op.in]: paymentItemIds } } });
    }

    updateItem(payment_item_id, item) {
        return this.schema.update(item, { where: { payment_item_id: payment_item_id } })
    }

}





module.exports = PaymentItem;