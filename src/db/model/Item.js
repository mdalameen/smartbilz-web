const Sequelize = require('sequelize');
const query = require('../query/itemQuery');
const StockController = require('../controller/StockController');
const OpeningStock = require('./OpeningStock');
const Unit = require('./Unit');

class Item {
    constructor(dynamicDb) {
        this.db = dynamicDb;
        this.schema = this.db.getConnection().define('item', {
            item_id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            item_name: Sequelize.STRING(100),
            item_type: {
                type: Sequelize.ENUM,
                values: ['GOODS', 'SERVICE']
            },
            unit_id: Sequelize.INTEGER,
            sku: Sequelize.STRING(100),
            item_code: Sequelize.STRING(100),
            hsnsac: Sequelize.STRING(100),
            mrp_price: Sequelize.DOUBLE,
            sales_price: Sequelize.DOUBLE,
            purchase_price: Sequelize.DOUBLE,
            is_taxable: Sequelize.BOOLEAN,
            tax_id: Sequelize.INTEGER,
            // cess_percent: Sequelize.DOUBLE,
            is_inventory: Sequelize.BOOLEAN,
            has_expiry_date: Sequelize.BOOLEAN,
            notify_expiry: Sequelize.INTEGER
        });
    }

    async addItem(item) {
        let opening_stock = item.opening_stock;
        delete item.opening_stock;
        let opening_stock_expiry_date = item.opening_stock_expiry_date;
        delete item.opening_stock_expiry_date;
        let mUnit = new Unit(this.db);

        let unit = await mUnit.getUnit(item.unit_id);
        // if (unit)
        //     item.unit_notation = unit.unit_notation;
        const result = await this.schema.create(item);
        console.log(result.dataValues.item_id);
        console.log(opening_stock);
        console.log('opening_stock');
        console.log('opening_stock_expiry_date');
        console.log(opening_stock_expiry_date);

        if (item.is_inventory) {
            let expiry_date;
            if (item.has_expiry_date)
                expiry_date = opening_stock_expiry_date;

            const mStockController = new StockController(this.db);



            let conversion = unit.conversion_rate;
            let calculatedStock = opening_stock * conversion;
            await mStockController.addOpeningStock(result.dataValues.item_id, calculatedStock, expiry_date);
        }


        return result;
    }

    async updateItem(item, item_id) {
        let opening_stock = item.opening_stock;
        delete item.opening_stock;
        let opening_stock_expiry_date = item.opening_stock_expiry_date;
        delete item.opening_stock_expiry_date;
        const mOpenStock = new OpeningStock(this.db);
        const stock = await mOpenStock.get(item_id);
        console.log('item_id');
        console.log(item_id);
        console.log('stock');
        console.log(stock);


        const mStockController = new StockController(this.db);
        if (item.is_inventory) {
            let expiry_date;
            if (item.has_expiry_date)
                expiry_date = opening_stock_expiry_date;
            const mUnit = new Unit(this.db);

            let quantity = mUnit.conversion_rate * opening_stock;
            if (stock) {
                await mStockController.updateOpeningStock(stock.dataValues.in_stock_id, quantity, expiry_date);
            } else {
                await mStockController.addOpeningStock(item_id, quantity, expiry_date);
            }
        } else {
            await mStockController.deleteOpeningStock(item_id);
        }

        return this.schema.update(item, { where: { item_id: item_id } });
    }

    async deleteItem(item_id) {
        const mStockController = new StockController(this.db);
        await mStockController.deleteOpeningStock(item_id);
        return this.schema.destroy({ where: { item_id: item_id } });
    }

    async getItemList(limit, offset) {
        let countResult = await this.db.getConnection().query('select count(*) from items;', { type: Sequelize.QueryTypes.SELECT });
        let count = 0;
        for (var countItem of countResult)
            count = countItem.count;

        let dataList = await this.db.getConnection().query(query.getItemListQuery(limit, offset), { type: Sequelize.QueryTypes.SELECT });
        return { count: count, data: dataList };
    }

    async getSelectedItems(items) {
        const result = await this.schema.findAll({ where: { item_id: { [Sequelize.Op.in]: items } } });
        let resultItems = [];
        for (let r of result)
            resultItems.push(r.dataValues);
        return resultItems;
    }
    async getSelectedItemsMap(items) {
        const result = await this.schema.findAll({ where: { item_id: { [Sequelize.Op.in]: items } } });
        let map = new Map();
        for (let r of result)
            map.set(r.dataValues.item_id, r.dataValues);
        return map;
    }

    async getItemById(item_id) {
        let dataList = await this.db.getConnection().query(query.getItem(item_id), { type: Sequelize.QueryTypes.SELECT });
        console.log(dataList);
        if (dataList.length > 0)
            return dataList[0];
        // return dataList;
        // return this.schema.findOne({ where: { item_id: item_id } });
    }

    getSchema() {
        return this.schema;
    }

    async deleteValidation(item_id) {
        let countResult = await this.db.getConnection().query(query.deleteValidation(item_id), { type: Sequelize.QueryTypes.SELECT });
        if (countResult[0].group_items >= 0)
            return "Delete item from group first";

        if (countResult[0].invoice_items >= 0)
            return "Delete item from Sales Invoice first";

        if (countResult[0].purchaseItems >= 0)
            return "Delete item from Purchase Invoice first";
    }

}

module.exports = Item;