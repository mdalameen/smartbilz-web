const Sequelize = require('sequelize');
const query = require('../query/purchaseQuery');
const Contact = require('./Contact');
const InvoiceItem = require('./InvoiceItem');
const PuInvoiceItem = require('./PuInvoiceItem');

class PurchaseInvoice {
    constructor(dynamicDb) {
        this.db = dynamicDb;
        this.schema = this.db.getConnection().define('purchase_invoice', {
            pu_invoice_id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            }, pu_invoice_number: {
                type: Sequelize.STRING(100),
                allowNull: false
            }, contact_id: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            pu_invoice_date: Sequelize.DATEONLY,
            terms: Sequelize.INTEGER,
            due_date: Sequelize.DATEONLY,
            order_number: Sequelize.STRING(100),
            notes: Sequelize.TEXT,
            pu_invoice_amount: Sequelize.DOUBLE,
            payment_status: {
                type: Sequelize.ENUM,
                values: ['PAID', 'UNPAID'],
                allowNull: false
            },
            pu_invoice_status: {
                type: Sequelize.ENUM,
                values: ['DRAFT', 'LIVE']
            }
        });
    }

    getSchema() { return this.schema; }


    // async validateNumber(pu_invoice_number, pu_invoice_id) {
    //     let output = await this.db.getConnection().query(query.getPuInvoiceNumberCount(pu_invoice_number, pu_invoice_id), { type: Sequelize.QueryTypes.SELECT });
    //     let result = false;
    //     if (output[0].count > 0)
    //         result = true;
    //     console.log(output);
    //     console.log(result);
    //     return result;
    // }

    async validateVendor(vendorId) {
        let contact = new Contact(this.db);
        let customer = await contact.getContact(vendorId);
        if (customer)
            return customer.type != 'VENDOR';
        return true;
    }

    async add(puInvoice) {
        let result = await this.schema.create(puInvoice);
        return result.get({ plane: true });
    }

    async validateHasPuInvoice(pu_invoice_id) {
        const countResult = await this.db.getConnection().query(query.hasPuInvoice(pu_invoice_id), { type: Sequelize.QueryTypes.SELECT });
        console.log(countResult[0].count);
        return countResult[0].count > 0;
    }

    updateInvoice(pu_invoice_id, puInvoice) {
        return this.schema.update(puInvoice, { where: { pu_invoice_id: pu_invoice_id } });
    }

    async getPuInvoiceById(pu_invoice_id) {

        let invList = await this.db.getConnection().query(query.getPuInvoiceById(pu_invoice_id), { type: Sequelize.QueryTypes.SELECT });
        if (invList.length > 0) {

            let inv = invList[0];
            const invItems = new PuInvoiceItem(this.db);
            let itemList = await invItems.getList(pu_invoice_id);
            console.log('>>>>>>>>> itemList');
            console.log(itemList);
            inv.pu_invoice_items = itemList;
            return inv;
        }

        // if (invList) {
        //     const invItems = new PuInvoiceItem(this.db);
        //     let itemList = await invItems.getList(pu_invoice_id);
        //     console.log(itemList);
        //     invList.pu_invoice_items = itemList;
        // }
        return null;
    }

    async deleteInvoiceById(pu_invoice_id) {
        await this.schema.destroy({ where: { pu_invoice_id: pu_invoice_id } });
        const invItems = new PuInvoiceItem(this.db);
        await invItems.deleteByInvoiceId(pu_invoice_id);
    }

    async list(offset, limit) {
        let cResult = await this.db.getConnection().query(query.getPuInvoiceCount(), { type: Sequelize.QueryTypes.SELECT });
        let count = cResult[0].count;
        let lResult = await this.db.getConnection().query(query.getPurchaseInvoiceList(offset, limit), { type: Sequelize.QueryTypes.SELECT });
        return { count: count, data: lResult };
    }

    updateStatus(pu_invoice_id, status) {
        if (!(status == 'PAID' || status == 'UNPAID'))
            new Error("Invalid payment status");
        return this.schema.update({ payment_status: status }, { where: { pu_invoice_id: pu_invoice_id } });
    }

    validateAmount(purchase_invoice, taxes) {
        let invoice_amount = 0;
        for (let item of purchase_invoice.pu_invoice_items) {
            let cSubTotal = ((item.quantity * item.price) - item.discount_amount);
            if (cSubTotal != item.sub_total)
                return "Incorrect Item, quantity, price & subtotal value";
            let cgst = item.cgst || 0;
            let igst = item.igst || 0;
            let sgst = item.sgst || 0;
            let tax_amount = item.tax_amount || 0;
            let taxPercentage = 0;
            for (var t of taxes)
                if (t.tax_id == item.tax_id)
                    taxPercentage = t.percentage;
            console.log('taxPercentage');
            console.log(taxPercentage);

            console.log(`cgst${cgst}`);
            console.log(`igst${igst}`);
            console.log(`sgst${sgst}`);
            console.log(`tax_amount${tax_amount}`);

            if (cgst > 0 && igst > 0 && sgst > 0)
                return "Incorrect gst amount";

            if (cgst != sgst)
                return "Incorrect cgst & sgst";

            if (cgst > 0 && sgst > 0)
                taxPercentage = taxPercentage;

            console.log((cSubTotal / 100) * taxPercentage);
            console.log(tax_amount);
            if (((cSubTotal / 100) * taxPercentage) != tax_amount)
                return "Incorrect tax percentage";

            if ((cgst + igst + sgst) != tax_amount)
                return "Incorrect tax amount";

            invoice_amount += (cSubTotal + tax_amount);
        }

        if (invoice_amount != purchase_invoice.pu_invoice_amount)
            return "Incorrect invoice amount";
    }

    async validateDelete(pu_invoice_id) {
        console.log('started validateDelete');
        let result = await this.db.getConnection().query(query.invoiceDeleteValidation(pu_invoice_id), { type: Sequelize.QueryTypes.SELECT });
        console.log('result is')
        console.log(result);;
        if (result[0].count > 0)
            return "Delete Receipt of this invoice first";
    }
}

module.exports = PurchaseInvoice;