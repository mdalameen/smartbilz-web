const Sequelize = require('sequelize');

class InStock {
    constructor(dynamicDb) {
        this.db = dynamicDb;
        this.schema = this.db.getConnection().define('in_stock', {
            in_stock_id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            item_id: Sequelize.INTEGER,
            quantity: Sequelize.DOUBLE,
            expiry_date: Sequelize.DATEONLY
        });
    }

    getSchema() {
        return this.schema;
    }

    addBulk(stocks) {
        return this.schema.bulkCreate(stocks, { raw: true });
    }

    add(stock) {
        return this.schema.create(stock, { raw: true });
    }

    update(stock, in_stock_id) {
        return this.schema.update(stock, { where: { in_stock_id: in_stock_id } });
    }

    delete(in_stock_id) {
        return this.schema.destroy({ where: { in_stock_id: in_stock_id } });
    }

    deleteAll(inStockIds) {
        return this.schema.destroy({ where: { in_stock_id: { [Sequelize.Op.in]: inStockIds } } });
    }
}

module.exports = InStock;