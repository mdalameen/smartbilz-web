const sequelize = require('sequelize');
const connection = require('../connection');

class Client {
    constructor() {
        this.client = connection.define('client', {
            client_id: {
                type: sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            email: {
                type: sequelize.STRING,
                unique: true
            },
            company_name: {
                type: sequelize.STRING
            },
            password: {
                type: sequelize.STRING(1000),
                allowNull: false,
            },
            db_name: {
                type: sequelize.STRING,
                allowNull: false,
            },
            dev1: {
                type: sequelize.STRING,
                allowNull: true,
            },
            dev2: {
                type: sequelize.STRING,
                allowNull: true,
            },
            dev3: {
                type: sequelize.STRING,
                allowNull: true,
            },
            agent_code: {
                type: sequelize.INTEGER
            },
            reg_date: {
                type: sequelize.DATE
            }
        });
    }

    getAllClients() {
        return this.client.findAll({ raw: true });
    }

    getSchema() {
        return this.client;
    }

    getClientByEmail(email) {
        return this.client.findOne({ where: { email: email } });
    }

    addClient(email, password, company_name, db_name, dev1, agent_code, reg_date) {
        return this.client.create({
            email: email,
            password: password,
            company_name: company_name,
            db_name: db_name,
            dev1: dev1,
            agent_code: agent_code,
            reg_date: reg_date
        });
    }

    hasLoggedIn(code) {

    }

    forgotPassword(client_id) {

    }
}

module.exports = Client;