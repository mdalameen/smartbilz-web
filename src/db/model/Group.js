const Sequelize = require('sequelize');
const query = require('../query/itemQuery');
const group_item = require('./GroupItem');

class Group {
    constructor(dynamicDb) {
        this.dynamicDb = dynamicDb;
        this.schema = this.dynamicDb.getConnection().define('group', {
            group_id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            }, group_name: {
                type: Sequelize.STRING(100)
            }, group_discount_percent: {
                type: Sequelize.INTEGER
            }
        });
    }

    async getGroups() {
        let result = await this.dynamicDb.getConnection().query(query.getGroups(), { type: Sequelize.QueryTypes.SELECT });
        console.log(result);
        let map = new Map();
        for (var v of result) {
            if (map.get(`${v.group_id}`)) {
                let group_item = map.get(`${v.group_id}`);
                if (v.group_item_id)
                    group_item.items.push({ group_item_id: v.group_item_id, item_id: v.item_id, item_name: v.item_name, sales_price: v.sales_price });
                map.set(`${v.group_id}`, group_item);
            } else {
                let items = [];
                if (v.group_item_id)
                    items.push({ group_item_id: v.group_item_id, item_id: v.item_id, item_name: v.item_name, sales_price: v.sales_price });
                let group_item = { group_id: v.group_id, group_name: v.group_name, group_discount_percent: v.group_discount_percent, items: items };
                map.set(`${v.group_id}`, group_item);
            }
        }
        console.log("logging map");
        console.log(map)
        console.log("logging map values");
        console.log(map.values())
        let list = [];
        for (var mapItem of map.keys())
            list.push(map.get(mapItem));
        console.log("map array");
        console.log(list);
        return list;
    }

    getGroupCountByGroupName(group_name, group_id) {
        console.log('inside getGroupCOunt');
        return this.dynamicDb.getConnection().query(query.getGroupByName(group_name, group_id), { type: Sequelize.QueryTypes.SELECT });
    }

    addGroup(itemGroup) {
        return this.schema.create(itemGroup);
    }

    updateGroup(group, group_id) {
        return this.schema.update(group, { where: { group_id: group_id } });
    }

    async updateGroupItems(items, oldItems, group_id) {
        let deleteGroupItems = [];
        let addItems = [];
        let newGroupItemMap = new Map();

        for (var newItem of items) {
            if (newItem.group_item_id) {
                newGroupItemMap.set(`${newItem.group_item_id}`, newItem);
            } else
                addItems.push(newItem.item_id);
        }
        console.log(`newMapAdded ${newGroupItemMap}`);
        console.log(newGroupItemMap);

        for (var oldItem of oldItems) {
            let value = newGroupItemMap.get(`${oldItem.group_item_id}`);
            console.log(`value is ${value}`);
            if (!value)
                deleteGroupItems.push(oldItem.group_item_id);
        }
        console.log(`addItems${addItems}`);
        console.log(`deleteItems${deleteGroupItems}`);

        let mGroupItem = new group_item(this.dynamicDb);
        if (deleteGroupItems.length > 0) {
            let deletedItems = await mGroupItem.deleteGroupItemById(deleteGroupItems);
            console.log(deletedItems);
        }

        let toAddItems = [];
        for (var v of addItems)
            toAddItems.push({ group_id: group_id, item_id: v });
        console.log("addItems is " + toAddItems);
        if (addItems.length > 0) {
            let gItemResult = await mGroupItem.addMultipleGroupItem(toAddItems);
            console.log(gItemResult);
        }
    }

    async deleteGroup(group_id) {
        const mGroupItem = new group_item(this.dynamicDb);
        await mGroupItem.deleteGroupItem(group_id);
        return this.schema.destroy({ where: { group_id: group_id } });
    }

    async getGroupById(group_id) {
        let groupResult = await this.schema.findOne({ where: { group_id: group_id }, raw: true });
        if (groupResult) {
            let itemResult = await this.getGroupItemById(group_id);
            console.log(itemResult);
            groupResult.items = itemResult;
        }
        return groupResult;
    }

    getGroupItemById(group_id) {
        return this.dynamicDb.getConnection().query(query.getGroupItemById(group_id), { type: Sequelize.QueryTypes.SELECT });
    }

    async getGroupIdForItems(items, group_id) {
        const result = await this.dynamicDb.getConnection().query(query.getGroupByItem(items, group_id), { type: Sequelize.QueryTypes.SELECT });
        console.log(result);

        return result;
    }

    getSchema() {
        return this.schema;
    }

    async validate(group, group_id) {
        console.log('validating group');
        const groupNameResult = await this.getGroupCountByGroupName(group.group_name, group_id);
        console.log('validating group completed');
        console.log(groupNameResult);

        if (groupNameResult[0].count > 0)
            return `Group Name ${group.group_name} is already present`;

        let itemArray = this._getItemArray(group.items);
        if (group.items.length > 0) {
            console.log("inside Validating condition");
            console.log(itemArray);
            let iRes = await this.getGroupIdForItems(itemArray, group_id);
            console.log(iRes);
            if (iRes.length > 0)
                return `Item ${iRes[0].item_name} is already present in Group ${iRes[0].group_name}`;
        }
    }

    async addGroupItems(group_items, group_id) {
        let mGroupItem = new group_item(this.dynamicDb);
        let items = [];
        for (var v of group_items)
            items.push({ group_id: group_id, item_id: v.item_id });
        console.log("addItems is " + items);
        if (items.length > 0) {
            let gItemResult = await mGroupItem.addMultipleGroupItem(items);
            console.log(gItemResult);
        }
    }

    _getItemArray(items) {
        console.log(items);
        let i = [];
        for (var item of items)
            i.push(item.item_id);
        return i;
    }
}

module.exports = Group;