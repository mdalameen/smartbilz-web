const Sequelize = require('sequelize');

class OutStock {
    constructor(dynamicDb) {
        this.db = dynamicDb;
        this.schema = this.db.getConnection().define('out_stock', {
            out_stock_id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            item_id: Sequelize.INTEGER,
            quantity: Sequelize.DOUBLE
        });
    }

    getSchema() {
        return this.schema;
    }

    addBulk(stocks) {
        return this.schema.bulkCreate(stocks, { raw: true });
    }

    add(stock) {
        return this.schema.create(stock, { raw: true });
    }

    update(stock, out_stock_id) {
        return this.schema.update(stock, { where: { out_stock_id: out_stock_id } });
    }

    delete(out_stock_id) {
        return this.schema.destroy({ where: { out_stock_id: out_stock_id } });
    }

    deleteAll(outStockIds) {
        return this.schema.destroy({ where: { out_stock_id: { [Sequelize.Op.in]: outStockIds } } });
    }
}

module.exports = OutStock;