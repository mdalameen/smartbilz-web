const Sequelize = require('sequelize');
const StockController = require('../controller/StockController');
const Item = require('./Item');

class PuInvoiceItem {
    constructor(dynamicDb) {
        this.db = dynamicDb;
        this.schema = this.db.getConnection().define('purchase_invoice_item', {
            pu_invoice_item_id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            }, pu_invoice_id: {
                type: Sequelize.INTEGER,
                allowNull: false
            }, item_id: {
                type: Sequelize.INTEGER,
                allowNull: false
            }, item_name: Sequelize.STRING(100),
            // tax_percent: Sequelize.DOUBLE,
            item_desc: Sequelize.STRING(200),
            is_goods: Sequelize.BOOLEAN,
            tax_id: Sequelize.DOUBLE,
            tax_desc: Sequelize.DOUBLE,
            igst: Sequelize.DOUBLE,
            cgst: Sequelize.DOUBLE,
            sgst: Sequelize.DOUBLE,
            quantity: Sequelize.DOUBLE,
            price: Sequelize.DOUBLE,
            discount_amount: Sequelize.DOUBLE,
            is_discount_type_percent: Sequelize.BOOLEAN,
            sub_total: Sequelize.DOUBLE,
            tax_amount: Sequelize.DOUBLE,
            unit_id: Sequelize.INTEGER
        });
    }

    getSchema() { return this.schema };

    async addAllItems(pu_invoice_id, items) {

        console.log('ToaddedItems');
        console.log(items);

        for (let item of items)
            item.pu_invoice_id = pu_invoice_id;

        let result = await this.schema.bulkCreate(items, { returning: true });
        console.log('result');
        console.log(result);

        let addedItems = [];
        for (let resultItem of result)
            addedItems.push(resultItem.dataValues);

        console.log('addedItems');
        console.log(addedItems);

        const mStockController = new StockController(this.db);
        const mItem = new Item(this.db);
        await mStockController.addPuInvoiceItems(addedItems, mItem);

        console.log('insert result');
        console.log(result);

        return result;
    }

    getList(pu_invoice_id) {
        return this.schema.findAll({ where: { pu_invoice_id: pu_invoice_id }, raw: true });
    }

    updateItem(pu_invoice_item_id, item) {
        return this.schema.update(item, { where: { pu_invoice_item_id: pu_invoice_item_id } });
    }

    async updateInvoiceItems(pu_invoice_id, items) {
        let oldItems = await this.getList(pu_invoice_id);
        let deleteItems = [];
        let addItems = [];
        let updateItems = [];

        let allItemMap = new Map();

        for (var nItem of items) {
            if (nItem.pu_invoice_item_id) {
                allItemMap.set(nItem.pu_invoice_item_id, nItem);
            } else
                addItems.push(nItem);
        }
        console.log(`allItemsInMap`);
        console.log(allItemMap);

        for (var oItem of oldItems) {
            let mapItem = allItemMap.get(oItem.pu_invoice_item_id);
            if (mapItem)
                updateItems.push(mapItem);
            else
                deleteItems.push(oItem);
        }

        const mStockController = new StockController(this.db);

        console.log(`updatedItems arrays`);
        console.log(updateItems);
        console.log(`addItems arrays`);
        console.log(addItems);
        console.log(`deleteItems arrays`);
        console.log(deleteItems);
        if (deleteItems.length > 0) {
            let delResult = await this._deleteItems(deleteItems);
            const mItem = new Item(this.db);
            await mStockController.deletePuInvoiceItems(deleteItems, mItem);
            console.log(`=========================\ndeleteResult\n${delResult}`);
        }

        if (addItems.length > 0) {
            let insResult = await this.addAllItems(pu_invoice_id, addItems);
            console.log(`=========================\ninsertResult`);
            console.log(insResult);
        }

        for (var updateItem of updateItems) {
            let pu_invoice_item_id = updateItem.pu_invoice_item_id;
            delete updateItem.pu_invoice_item_id;
            console.log(`updateInvoiceItemIs:${pu_invoice_item_id}`);
            console.log(`updatedInItemIs:${updateItem}`);
            let updateResult = await this.updateItem(pu_invoice_item_id, updateItem);
            console.log(`=========================\nupdateResult\n${updateResult}`);
            updateItem.pu_invoice_item_id = pu_invoice_item_id;
        }
        if (updateItems.length > 0) {
            const mItem = new Item(this.db);
            await mStockController.deletePuInvoiceItems(updateItems, mItem);
            await mStockController.addPuInvoiceItems(updateItems, mItem);
        }
    }


    async _deleteItems(pu_invoice_items) {
        let items = [];
        for (let item of pu_invoice_items)
            items.push(item.pu_invoice_item_id);
        return this.schema.destroy({ where: { pu_invoice_item_id: { [Sequelize.Op.in]: items } } });
    }

    async deleteByInvoiceId(pu_invoice_id) {
        console.log("delete by invoice_id");
        console.log(pu_invoice_id);
        const items = await this.getList(pu_invoice_id);
        const result = await this.schema.destroy({ where: { pu_invoice_id: pu_invoice_id } })
        console.log('items');
        console.log(items);
        let invoice_items = [];
        for (let invoiceItem of items)
            invoice_items.push(invoiceItem);
        console.log('invoice_items');
        console.log(invoice_items);
        const mStockController = new StockController(this.db);
        const mItem = new Item(this.db);
        await mStockController.deletePuInvoiceItems(invoice_items, mItem);
        return result;
    }
}

module.exports = PuInvoiceItem;