const Sequelize = require('sequelize');
const contactQuery = require('../query/contactQuery');

class Contact {
    constructor(DynamicDb) {
        this.db = DynamicDb;
        this.schema = this.db.getConnection().define('contact', {
            contact_id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            }, contact_name: {
                type: Sequelize.STRING(200),
                allowNull: false
            }, display_name: {
                type: Sequelize.STRING(200)
            }, type: {
                type: Sequelize.ENUM,
                values: ['CUSTOMER', 'VENDOR']
            }, email: {
                type: Sequelize.STRING(100)
            }, telephone: {
                type: Sequelize.STRING(20)
            }, mobile: {
                type: Sequelize.STRING(20),
            }, website: {
                type: Sequelize.STRING(200),
            }, gst_type: {
                type: Sequelize.ENUM,
                values: ['TAX_TYPE_GST_REGULAR', 'TAX_TYPE_GST_COMPOSITION', 'TAX_TYPE_UNREGISTERED', 'TAX_TYPE_CONSUMER']
            }, gstin: {
                type: Sequelize.STRING(15),
            }, place_of_supply: {
                type: Sequelize.STRING,
            }, place_of_supply_code: {
                type: Sequelize.INTEGER,
            }, payment_terms: {
                type: Sequelize.INTEGER,
            }, address_line_1: {
                type: Sequelize.STRING(100),
            }, address_line_2: {
                type: Sequelize.STRING(100),
            }, city: {
                type: Sequelize.STRING(100),
            }, state: {
                type: Sequelize.STRING(100),
            }, pincode: {
                type: Sequelize.STRING(10),
            }
        });
    }

    getSchema() {
        return this.schema;
    }

    addContact(name, display_name, type, email, telephone, mobile, website, gst_type, gstin, place_of_supply, place_of_supply_code, payment_terms, address_line_1, address_line_2, city, state, pincode) {
        let data = {
            contact_name: name,
            display_name: display_name,
            type: type,
            email: email,
            telephone: telephone,
            mobile: mobile,
            website: website,
            gst_type: gst_type,
            gstin: gstin,
            place_of_supply: place_of_supply,
            place_of_supply_code: place_of_supply_code,
            payment_terms: payment_terms,
            address_line_1: address_line_1,
            address_line_2: address_line_2,
            city: city,
            state: state,
            pincode: pincode
        };
        return this.getSchema().create(data);
    }

    async getAllContactsMeta(offset, limit, type) {
        let cResult = await this.db.getConnection().query(contactQuery.getAllContactsCount(type), { type: Sequelize.QueryTypes.SELECT });
        let count = cResult[0].count;
        let lResult = await this.db.getConnection().query(contactQuery.getAllContacts(offset, limit, type), { type: Sequelize.QueryTypes.SELECT });
        return { count: count, data: lResult };
    }

    getContact(contact_id) {
        return this.getSchema().findOne({ where: { contact_id: contact_id }, raw: true });
    }

    updateContacts(contact_id, name, display_name, type, email, telephone, mobile, website, gst_type, gstin, place_of_supply, place_of_supply_code, payment_terms, address_line_1, address_line_2, city, state, pincode) {
        let data = {
            contact_name: name,
            display_name: display_name,
            type: type,
            email: email,
            telephone: telephone,
            mobile: mobile,
            website: website,
            gst_type: gst_type,
            gstin: gstin,
            place_of_supply: place_of_supply,
            place_of_supply_code: place_of_supply_code,
            payment_terms: payment_terms,
            address_line_1: address_line_1,
            address_line_2: address_line_2,
            city: city,
            state: state,
            pincode: pincode
        };
        return this.getSchema().update(data, { where: { contact_id: contact_id } });
    }
}

module.exports = Contact;