const sequelize = require('sequelize');

class group_item {
    constructor(dynamicDb) {
        this.dynamicDb = dynamicDb;
        this.schema = this.dynamicDb.getConnection().define('group_item', {
            group_item_id: {
                type: sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            }, group_id: {
                type: sequelize.INTEGER
            }, item_id: {
                type: sequelize.INTEGER
            }
        });
    }

    async getGroupItems() {
        let result = await this.schema.findAndCountAll();
        let list = [];
        let count = result.count;
        for (v of result.rows)
            list.push(v.dataValues);
        return { count: count, data: list };
    }

    addMultipleGroupItem(items) {
        return this.schema.bulkCreate(items);
    }

    addGroupItem(itemGroup) {
        return this.schema.create(itemGroup);
    }

    updateGroupItem(itemGroup, group_id) {
        return this.schema.update(itemGroup, { where: { group_id: group_id } });
    }

    deleteGroupItem(group_id) {
        return this.schema.destroy({ where: { group_id: group_id } });
    }

    deleteGroupItemById(group_item_id) {
        return this.schema.destroy({ where: { group_item_id: group_item_id } });
    }

    getGroupItemById(group_id) {
        return this.schema.findOne({ where: { group_id: group_id } });
    }

    getSchema() {
        return this.schema;
    }


}

module.exports = group_item;