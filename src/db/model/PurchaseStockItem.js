const Sequelize = require('sequelize');

class PurchaseStockItem {
    constructor(dynamicDb) {
        this.db = dynamicDb;
        this.schema = this.db.getConnection().define('purchase_stock_item', {
            pu_invoice_stock_item_id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            pu_invoice_item_id: Sequelize.INTEGER,
            in_stock_id: Sequelize.INTEGER
        });
    }

    getSchema() {
        return this.schema;
    }

    add(pu_invoice_item_id, in_stock_id) {
        return this.schema.create({ pu_invoice_item_id: pu_invoice_item_id, in_stock_id: in_stock_id });
    }

    async getPuInvoiceStockItemList(puInvoiceItemIds) {
        const result = await this.schema.findAll({ where: { pu_invoice_item_id: { [Sequelize.Op.in]: puInvoiceItemIds } } });
        let invoice_stock_items = [];
        for (let r of result)
            invoice_stock_items.push(r.dataValues);
        return invoice_stock_items;
    }

    deleteByInvoiceItemId(puInvoiceItemIds) {
        return this.schema.destroy({ where: { pu_invoice_item_id: { [Sequelize.Op.in]: puInvoiceItemIds } } });
    }
}

module.exports = PurchaseStockItem;