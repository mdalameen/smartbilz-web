const Sequelize = require('sequelize');

class Company {
    constructor(dynamicDb) {
        this.db = dynamicDb;
        this.schema = this.db.getConnection().define('company', {
            company_id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
            },
            place_of_supply_code: Sequelize.INTEGER,
            place_of_supply: Sequelize.STRING(100),
            gst_registration_type: {
                type: Sequelize.ENUM,
                values: ['TAX_TYPE_UNREGISTERED', 'TAX_TYPE_GST_REGULAR', 'TAX_TYPE_GST_COMPOSITION']
            },
            gstin: Sequelize.STRING(15),
            composition_tax_id: Sequelize.INTEGER
        });

    }

    getSchema() { return this.schema }

    async init() {
        await this.schema.sync({ force: true });
        await this.schema.create({
            company_id: 1,
            place_of_supply_code: 0,
            place_of_supply: null,
            gst_registration_type: null,
            gstin: null,
            composition_tax_id: null
        });
    }

    update(company) {
        return this.schema.update(company, { where: { company_id: 1 } });
    }

    validate(company) {
        if (company.gst_registration_type == 'TAX_TYPE_UNREGISTERED') {
            if (company.place_of_supply != null)
                return 'Place of supply should be null';
            if (company.place_of_supply_code != null)
                return 'Place of supply code should be null';
            if (company.gstin != null)
                return 'GSTIN should be null';
            if (company.composition_tax_id != null)
                return 'composition should be null';

        } else if (company.gst_registration_type == 'TAX_TYPE_GST_REGULAR') {
            if (company.place_of_supply == null || company.place_of_supply.length < 1)
                return 'Place of supply should not be empty';
            if (company.place_of_supply_code == null || company.place_of_supply_code < 1)
                return 'Place of supply code should be empty';
            if (company.gstin == null || company.gstin.length != 15)
                return 'GSTIN should 15 chars';
            if (company.composition_tax_id != null)
                return 'composition should be null';

        } else if (company.gst_registration_type == 'TAX_TYPE_GST_COMPOSITION') {
            if (company.place_of_supply == null || company.place_of_supply.length < 1)
                return 'Place of supply should not be empty';
            if (company.place_of_supply_code == null || company.place_of_supply_code < 1)
                return 'Place of supply code should be empty';
            if (company.gstin == null || company.gstin.length != 15)
                return 'GSTIN should 15 chars';
            if (company.composition_tax_id == null)
                return 'composition should not be null';
        }
    }

    async get() {
        let listResult = await this.schema.findOne({ where: { company_id: 1 } }, { plain: true });
        let result = listResult.dataValues;
        delete result.company_id;
        return result;
    }
}

module.exports = Company;