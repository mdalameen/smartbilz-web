const Sequelize = require('sequelize');
const decimalUtil = require('../util/decimalUtil');

const saleInvoiceSequenceName = "seq_sale_invoice_number";
const receiptSequenceName = "seq_receipt_number";
const purchaseInvoiceSequenceName = "seq_purchase_invoice_number";
const paymentSequenceName = "seq_payment_number";

// const saleInvoiceSequenceName = "seqSaleInvoiceNumber1";
// const receiptSequenceName = "seqReceiptNumber1";
// const purchaseInvoiceSequenceName = "seqPurchaseInvoiceNumber1";
// const paymentSequenceName = "seqPaymentNumber1";


class Sequence {
    constructor(DynamicDb) {
        this.db = DynamicDb;
    }

    async init() {
        await this._createSequence(saleInvoiceSequenceName);
        await this._createSequence(receiptSequenceName);
        await this._createSequence(purchaseInvoiceSequenceName);
        await this._createSequence(paymentSequenceName);
    }

    getNextSaleInvoiceNumber() {
        return this._getNextNumber(saleInvoiceSequenceName);
    }

    updateSaleInvoiceNumber(value) {
        return this._updateNumber(saleInvoiceSequenceName, value);
    }

    getNextReceiptNumber() {
        return this._getNextNumber(receiptSequenceName);
    }

    updateReceiptNumber(value) {
        return this._updateNumber(receiptSequenceName, value);
    }

    getNextPurchaseInvoiceNumber() {
        return this._getNextNumber(purchaseInvoiceSequenceName);
    }

    updatePurchaseInvoiceNumber(value) {
        return this._updateNumber(purchaseInvoiceSequenceName, value);
    }

    getNextPaymentNumber() {
        return this._getNextNumber(paymentSequenceName);
    }

    updatePaymentNumber(value) {
        return this._updateNumber(paymentSequenceName, value);
    }


    async _getNextNumber(sequenceName) {
        let number = await this._getCurrentSequenceNumber(sequenceName);
        return number;
    }

    async _updateNumber(sequenceName, value) {
        let newNumber = await this._getNextNumber(sequenceName);
        let currentNumber = decimalUtil.getFormattedInteger(value);
        if (currentNumber >= newNumber)
            return this._setSequenceNumber(sequenceName, currentNumber);
    }



    async _createSequence(sequenceName) {
        const result = await this.db.getConnection().query(`create sequence "${sequenceName}";`, { type: Sequelize.QueryTypes.RAW })
        console.log(result);
    }

    async _getNextSequenceNumber(sequenceName) {
        const result = await this.db.getConnection().query(`select nextval('"${sequenceName}"');`, { type: Sequelize.QueryTypes.SELECT });
        // console.log(result);
        // console.log(result[0].nextval);
        return result[0].nextval;
    }

    async _getCurrentSequenceNumber(sequenceName) {
        const result = await this.db.getConnection().query(`select last_value from "${sequenceName}";`, { type: Sequelize.QueryTypes.SELECT });
        console.log(result);
        return result[0].last_value;
    }

    async _setSequenceNumber(sequenceName, value) {
        let intValue = decimalUtil.getFormattedInteger(value) + 1;
        if (intValue == 0)
            return;
        if (intValue) {
            const result = await this.db.getConnection().query(`select setVal('"${sequenceName}"',${intValue});`, { type: Sequelize.QueryTypes.SELECT });
            // console.log(result);
            return result[0].setval;
        }
    }


}

module.exports = Sequence;