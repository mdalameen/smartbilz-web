const dat = require('date-and-time');
const dateFormat = 'YYYY-MM-DD';

module.exports.getPayableAndReceivable = (currentDate) => `
select
 (select coalesce((SUM("invoice_amount")-SUM("paid_amount")),0) as sales from "sale_invoices" where "payment_status"='UNPAID')
 -
 (select coalesce((sum(ri."amount")),0) as "sale_pay" from "receipts" r join "receipt_items" ri on r."receipt_id" = ri."receipt_id" where ri."invoice_id"  in (select "invoice_id" from "sale_invoices" where "payment_status"='UNPAID' )) 
 as "total_receivable",

 (select coalesce(SUM("pu_invoice_amount"), 0) as purchase from "purchase_invoices" where "payment_status"='UNPAID')
-
(select coalesce(sum(pi."amount"), 0) as "purchase_pay" from "payments" p join "payment_items" pi on p."payment_id" = pi."payment_id" where pi."pu_invoice_id"  in (select "pu_invoice_id" from "purchase_invoices" where "payment_status"='UNPAID'))
as "total_payable",

(select coalesce((SUM("invoice_amount")-SUM("paid_amount")), 0) as sales from "sale_invoices" where "payment_status"='UNPAID' and "due_date"<'${dat.format(currentDate, dateFormat)}')
-
(select coalesce(sum(ri."amount"), 0) as "salePay" from "receipts" r join "receipt_items" ri on r."receipt_id" = ri."receipt_id" where ri."invoice_id"  in (select "invoice_id" from "sale_invoices" where "payment_status"='UNPAID' and "due_date"<'${dat.format(currentDate, dateFormat)}')) 
 as "overdue_receivable", 
  
(select coalesce(SUM("pu_invoice_amount"), 0) as purchase from "purchase_invoices" where "payment_status"='UNPAID' and "due_date"<'${dat.format(currentDate, dateFormat)}')
-
(select coalesce(sum(pi."amount"), 0) as "purchase_pay" from "payments" p join "payment_items" pi on p."payment_id" = pi."payment_id" where pi."pu_invoice_id"  in (select "pu_invoice_id" from "purchase_invoices" where "payment_status"='UNPAID' and "due_date"<'${dat.format(currentDate, dateFormat)}'))
as "overdue_payable";
 ;
`;

module.exports.cashflow = (fromDate, toDate) => `
select case when r.month>0 then r.month else p.month end as month, coalesce(r.rec,0) - coalesce(p.pay,0) as "cash_flow" from  
(select sum("receipt_amount") as rec, date_part('MONTH', "receipt_date") as month from receipts where "receipt_date" between '${dat.format(fromDate, dateFormat)}' and '${dat.format(toDate, dateFormat)}' group by date_part('MONTH', "receipt_date")) r
full outer join 
(select sum("payment_amount") as pay, date_part('MONTH', "payment_date") as month from payments where "payment_date" between '${dat.format(fromDate, dateFormat)}' and '${dat.format(toDate, dateFormat)}' group by date_part('MONTH', "payment_date")) p
on r.month = p.month;
`;

module.exports.prevCashFlow = (date) => `
select 
coalesce((select sum("receipt_amount") from receipts where "receipt_date" < '${dat.format(date, dateFormat)}'), 0)-
coalesce((select sum("payment_amount") from payments where "payment_date" < '${dat.format(date, dateFormat)}'), 0) as "prevCashFlow";
`;

module.exports.incomeExpense = (fromDate, toDate) => `
select case when s.month>0 then s.month else p.month end as month, coalesce(s.income, 0) as income, coalesce(p.expense, 0) as "expense" from  
(select coalesce(sum("invoice_amount"), 0) as income, date_part('MONTH', "invoice_date") as month from "sale_invoices" where "invoice_date" between '${dat.format(fromDate, dateFormat)}' and '${dat.format(toDate, dateFormat)}' group by date_part('MONTH', "invoice_date")) s
full outer join 
(select coalesce(sum("pu_invoice_amount"), 0) as expense, date_part('MONTH', "pu_invoice_date") as month from "purchase_invoices" where "pu_invoice_date" between '${dat.format(fromDate, dateFormat)}' and '${dat.format(toDate, dateFormat)}' group by date_part('MONTH', "pu_invoice_date")) p
on s.month = p.month;
`;