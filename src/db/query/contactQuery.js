module.exports.getAllContactsCount = (type) => `select count("contact_id") from contacts ${getTypeString(type, true)};`;

function getTypeString(type, isCount) {
    var typeFilter = '';
    if (type) {
        typeFilter += `where ${isCount ? '' : 'c.'}type='`;
        if (type == 'CUSTOMER')
            typeFilter += "CUSTOMER'";
        else if (type == 'VENDOR')
            typeFilter += "VENDOR'";
    };
    return typeFilter
}

module.exports.getAllContacts = (offset, limit, type) => {
    return `
select c."contact_id", c."contact_name", c."display_name", c."type", c."gst_type", c."gstin", c."place_of_supply", c."place_of_supply_code", c."payment_terms", 
coalesce((coalesce(si."sales",0) - coalesce(sit."salePay",0)), 0) as "receivables", coalesce((coalesce(pi."purchase",0)) - coalesce(pit."purchasePay",0), 0) as "payables"
from contacts c 
LEFT JOIN
(select s."contact_id", SUM(s."invoice_amount")-SUM(s."paid_amount") as sales from "sale_invoices" s where s. "payment_status"='UNPAID' group by s."contact_id") 
si 
on c."contact_id" = si."contact_id" 
LEFT JOIN
(select r."contact_id", sum(ri."amount") as "salePay" from "receipts" r join "receipt_items" ri on r."receipt_id" = ri."receipt_id" where ri."invoice_id"  in (select ss."invoice_id" from "sale_invoices" ss where ss."payment_status"='UNPAID' )group by r."contact_id") 
sit 
on c."contact_id" = sit."contact_id" 
LEFT JOIN 
(select p."contact_id", SUM(p."pu_invoice_amount") as purchase from "purchase_invoices" p where p."payment_status"='UNPAID' group by p."contact_id") 
pi
on c."contact_id" = pi."contact_id" 
LEFT JOIN 
(select p."contact_id", sum(pi."amount") as "purchasePay" from "payments" p join "payment_items" pi on p."payment_id" = pi."payment_id" where pi."pu_invoice_id"  in (select pp."pu_invoice_id" from "purchase_invoices" pp where pp."payment_status"='UNPAID') group by p."contact_id")
pit 
on c."contact_id" = pit."contact_id"
${getTypeString(type, false)}
offset ${offset} limit ${limit}
;`
}