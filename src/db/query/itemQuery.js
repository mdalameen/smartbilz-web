const queryUtil = require('../../util/queryUtil');

module.exports.getItem = (itemId) => {
    let q = `
    select 
        i."item_id",
        i."item_name",
        i."item_type",
        u."unit_notation",
        u.unit_id,
        i."sku",
        i."item_code",
        i."hsnsac",
        i."mrp_price",
        i."sales_price",
        i."purchase_price",
        i."is_taxable",
        i."tax_id",
        i."is_inventory",
        i."has_expiry_date",
        i."notify_expiry" 
        FROM "items" i
        LEFT JOIN "group_items" gi ON i."item_id" = gi."item_id"
        LEFT JOIN "groups" g ON g."group_id" = gi."group_id"
        LEFT JOIN "units" u ON i."unit_id" = u."unit_id"
        where i.item_id =${itemId};
    `;
    return q;
}

module.exports.getItemListQuery = (limit, offset) => {
    let q =
        `SELECT i."item_id",
        i."item_name",
        i."item_type",
        u."unit_notation",
        u.unit_id,
        i."sku",
        i."item_code",
        i."hsnsac",
        i."mrp_price",
        i."sales_price",
        i."purchase_price",
        i."is_taxable",
        i."tax_id",
        i."is_inventory",
        i."has_expiry_date",
        i."notify_expiry",
        coalesce(g."group_discount_percent",0) as "group_discount_percent",
        (s."available_stock" * u."conversion_rate") as "available_stock"
    FROM "items" i
    LEFT JOIN "group_items" gi ON i."item_id" = gi."item_id"
    LEFT JOIN "groups" g ON g."group_id" = gi."group_id"
    LEFT JOIN "units" u ON i."unit_id" = u."unit_id" 
    LEFT JOIN (
                (SELECT coalesce(ins."in_stocks",0) - coalesce(out."out_stock",0) AS "available_stock",
                        CASE
                            WHEN ins."item_id">0 THEN ins."item_id"
                            ELSE out."item_id"
                        END AS "item_id"
                 FROM
                     (SELECT SUM(quantity) AS "in_stocks",
                             "item_id"
                      FROM "in_stocks"
                      GROUP BY "item_id") ins
                 FULL JOIN
                     (SELECT SUM(quantity) AS "out_stock",
                             "item_id"
                      FROM "out_stocks"
                      GROUP BY "item_id") OUT ON ins."item_id" = out."item_id")) s ON i."item_id"=s."item_id"
OFFSET ${offset}
LIMIT ${limit};`;
    // let s = `SELECT i."item_id", i."item_name", i."item_type", i."unit", i."sku", i."item_code", i."hsnsac", i."mrp_price", i."sales_price", i."purchase_price", i."is_taxable", i."tax_percent", i."cess_percent", i."is_inventory", i."hasExpiryData", i."notify_expiry", g."group_discount_percent" FROM "items" i LEFT JOIN "group_items" gi ON i."item_id" = gi."item_id" LEFT JOIN "groups" g ON g."group_id" = gi."group_id" OFFSET ${offset} LIMIT ${limit};`;
    return q;
};

module.exports.getGroups = () => 'select g."group_id", g."group_name", g."group_discount_percent", gi."group_item_id", gi."item_id", i."item_name", i."sales_price" from "groups" g left join "group_items" gi on g."group_id" = gi."group_id" left join "items" i on i."item_id" = gi."item_id";';

module.exports.getGroupByItem = (itemIds, group_id) => {
    let q = 'select g."group_id", gi."item_id" , i."item_name", g."group_name" from "groups" g INNER JOIN "group_items" gi on g."group_id" = gi."group_id" LEFT JOIN "items" i on i."item_id" = gi."item_id" ';
    console.log(`isValidArray ${queryUtil.isValidArray(itemIds)}`);
    if (queryUtil.isValidArray(itemIds)) {
        let itemArrayValue = queryUtil.getArrayStringValue(itemIds);
        console.log(`itemArrayValue is ${itemArrayValue}`);
        q = q + ` where gi."item_id" in (${itemArrayValue})`;
        if (group_id)
            q = q + ' AND';
    }

    if (group_id) {
        if (!queryUtil.isValidArray(itemIds))
            q = q + " where";
        q = q + ` g."group_id" not in (${group_id})`;
    }
    q = q + ';';
    console.log(`query is ${q}`);
    return q;
}

module.exports.getGroupByName = (group_name, group_id) => {
    let q = `select count(*) from "groups"  where "group_name" = '${group_name}'`;
    if (group_id)
        q = q + ` AND "group_id" not in(${group_id});`;
    else q = q + ';';
    return q;
}

module.exports.getGroupItemById = (group_id) => {
    return `select gi."group_item_id", gi."item_id", i."item_name", i."sales_price" from "groups" g INNER JOIN "group_items" gi on gi."group_id" = g."group_id" LEFT JOIN "items" i on gi."item_id" = i."item_id" where g."group_id" =${group_id};`;
}

module.exports.deleteValidation = (item_id) => `
select 
(select count("group_item_id") from "group_items" where "item_id" = ${item_id}) as "group_items",
(select count("invoice_item_id") from "sale_invoice_items" where "item_id"=${item_id}) as "invoice_items",
(select count("pu_invoice_item_id") from "purchase_invoice_items" where "item_id"=${item_id}) as "purchaseItems";
`
    ;