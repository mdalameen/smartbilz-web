const queryUtil = require('../../util/queryUtil');

module.exports.getInvoiceNumberCount = (invoice_number, invoice_id) => {
    let q = `select count(*) from "sale_invoices" where "invoice_number" = '${invoice_number}'`;
    if (invoice_id)
        q = q + ` AND "invoice_id" not in (${invoice_id});`;
    else
        q = q + `;`;
    return q;
}

module.exports.hasInvoice = (invoice_id) => `select count(*) from "sale_invoices" where "invoice_id" = ${invoice_id};`

module.exports.getReceiptNumberCount = (receipt_number, receipt_id) => {
    let q = `select count(*) from "receipts" where "receipt_number" = '${receipt_number}'`;
    if (receipt_id)
        q = q + ` AND "receipt_id" not in (${receipt_id});`;
    else
        q = q + `;`;
    return q;
}

module.exports.getInvoiceCount = () => `select count("invoice_id") from "sale_invoices";`;

module.exports.getInvoiceList = (offset, limit) => {
    let q = `select i."invoice_id", i."invoice_number", c."contact_name", c."display_name", i."due_date", i."invoice_date", i."invoice_amount", i."payment_status", i."invoice_status", i."is_retail_invoice" from "sale_invoices" i join "contacts" c on c."contact_id" = i."contact_id" order by i."invoice_id" desc limit ${limit} offset ${offset};`;
    return q;
}

module.exports.getInvoiceById = (invoice_id) => `SELECT i."invoice_id", i."invoice_number", i."contact_id", i."invoice_date", i."terms", i."due_date", i."order_number", i."notes", i."invoice_amount", i."paid_amount", i."payment_status", i."invoice_status", i."is_retail_invoice", c.place_of_supply_code, c.contact_name, c.display_name FROM "sale_invoices" i join contacts c on i.contact_id = c.contact_id WHERE i."invoice_id" = ${invoice_id};`

module.exports.getPendingAmount = (invoiceIds, receipt_id) => {
    let rq = '';
    if (receipt_id)
        rq = `where "receipt_id" not in (${receipt_id}) `;
    let q = `select i."invoice_id", (i."invoice_amount" - i."paid_amount" - coalesce(r."receipt_amount",0)) as "pendingAmount" from "sale_invoices" i LEFT JOIN (select "invoice_id", SUM("amount") as "receipt_amount" from "receipt_items" ${rq}GROUP BY "invoice_id") r ON i."invoice_id" = r."invoice_id" where i."invoice_id"`;
    if (invoiceIds)
        q = q + ` in (${queryUtil.getArrayStringValue(invoiceIds)});`;
    else
        q = q + ';';
    return q;
}

module.exports.receiptList = (offset, limit) => {
    let q = `select r."receipt_id", r."receipt_number", r."receipt_date", r."receipt_amount", r."payment_mode", c."contact_name", c."display_name" from "receipts" r join "contacts" c on c."contact_id" = r."contact_id" order by r."receipt_id" limit ${limit} offset ${offset};`
    return q;
}

module.exports.getReceipt = (receipt_id) => `SELECT r."receipt_id", r."receipt_number", r."contact_id", c.contact_name, c.display_name, r."receipt_date", r."receipt_amount", r."unused_receipt_amount", r."payment_mode", r."reference_number" FROM "receipts" r join contacts c on r.contact_id = c.contact_id WHERE r."receipt_id" = ${receipt_id};`;

module.exports.getReceiptCount = () => `select count("receipt_id") from "receipts";`;

module.exports.getPendingInvoiceListForContact = (contact_id) => `select i."invoice_id", i."invoice_amount", (i."invoice_amount" - i."paid_amount" - coalesce(r."receipt_amount",0)) as "pending_amount", i."invoice_number", i."invoice_date" from "sale_invoices" i LEFT JOIN (select "invoice_id", SUM("amount") as "receipt_amount" from "receipt_items" GROUP BY "invoice_id") r ON i."invoice_id" = r."invoice_id" where i."contact_id" = ${contact_id} and i."payment_status" = 'UNPAID';`

module.exports.invoiceDeleteValidation = (invoice_id) => `
select count("invoice_id") as count from "receipt_items" where "invoice_id"=${invoice_id};
`;