const queryUtil = require('../../util/queryUtil');

module.exports.getPuInvoiceNumberCount = (pu_invoice_number, pu_invoice_id) => {
    let q = `select count(*) from "purchase_invoices" where "pu_invoice_number" = '${pu_invoice_number}'`;
    if (pu_invoice_id)
        q = q + ` AND "pu_invoice_id" not in (${pu_invoice_id});`;
    else
        q = q + `;`;
    return q;
}

module.exports.hasPuInvoice = (pu_invoice_id) => `select count(*) from "purchase_invoices" where "pu_invoice_id" = ${pu_invoice_id};`

module.exports.getPaymentNumberCount = (payment_number, payment_id) => {
    let q = `select count(*) from "payments" where "payment_number" = '${payment_number}'`;
    if (payment_id)
        q = q + ` AND "payment_id" not in (${payment_id});`;
    else
        q = q + `;`;
    return q;
}

module.exports.getPuInvoiceCount = () => `select count("pu_invoice_id") from "purchase_invoices";`;

module.exports.getPuInvoiceById = (pu_invoice_id) => `SELECT p."pu_invoice_id", p."pu_invoice_number", p."contact_id", p."pu_invoice_date", p."terms", p."due_date", p."order_number", p."notes", p."pu_invoice_amount", p."payment_status", p."pu_invoice_status", c.place_of_supply_code, c.contact_name, c.display_name FROM "purchase_invoices" p join contacts c on p.contact_id = c.contact_id WHERE p."pu_invoice_id" = ${pu_invoice_id};`

module.exports.getPurchaseInvoiceList = (offset, limit) => {
    let q =
        `select 
    i."pu_invoice_id", 
    i."pu_invoice_number", 
    c."contact_name", 
    c."display_name", 
    i."due_date", 
    i."pu_invoice_date", 
    i."pu_invoice_amount", 
    i."payment_status", 
    i."pu_invoice_status" 
        from "purchase_invoices" i join "contacts" c on c."contact_id" = i."contact_id" 
        order by i."pu_invoice_id" desc limit ${limit} offset ${offset};`;
    return q;
}

module.exports.getPendingAmount = (puInvoiceIds, payment_id) => {
    let rq = '';
    if (payment_id)
        rq = `where "payment_id" not in (${payment_id}) `;
    let q = `select i."pu_invoice_id", (i."pu_invoice_amount" - coalesce(r."payment_amount",0)) as "pendingAmount" from "purchase_invoices" i LEFT JOIN (select "pu_invoice_id", SUM("amount") as "payment_amount" from "payment_items" ${rq}GROUP BY "pu_invoice_id") r ON i."pu_invoice_id" = r."pu_invoice_id" where i."pu_invoice_id"`;
    if (puInvoiceIds)
        q = q + ` in (${queryUtil.getArrayStringValue(puInvoiceIds)});`;
    else
        q = q + ';';
    return q;
}

module.exports.paymentList = (offset, limit) => {
    let q = `select r."payment_id", r."payment_number", r."payment_date", r."payment_amount", r."payment_mode", c."contact_name" from "payments" r join "contacts" c on c."contact_id" = r."contact_id" order by r."payment_id" limit ${limit} offset ${offset};`
    return q;
}

module.exports.getPaymentCount = () => `select count("payment_id") from "payments";`;

module.exports.getPendingPurchaseInvoiceListForContact = (contact_id) =>
    `select 
i."pu_invoice_id", 
(i."pu_invoice_amount" - coalesce(r."payment_amount",0)) as "pendingAmount", 
i."pu_invoice_number", 
i."pu_invoice_date" 
    from "purchase_invoices" i 
    LEFT JOIN (select "pu_invoice_id", SUM("amount") as "payment_amount" 
        from "payment_items" GROUP BY "pu_invoice_id") r 
        ON i."pu_invoice_id" = r."pu_invoice_id" where i."contact_id" = ${contact_id} and i."payment_status" = 'UNPAID';`


module.exports.invoiceDeleteValidation = (pu_invoice_id) => `
select count("pu_invoice_id") as count from "payment_items" where "pu_invoice_id"=${pu_invoice_id};
`;