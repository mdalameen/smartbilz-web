const Sequelize = require('sequelize');
config = require('config');

const connection = new Sequelize(config.get('db'), config.get("dbUserName"), config.get('dbPassword'), {
    host: config.get('dbHost'),
    dialect: 'postgres',
    operatorsAliases: false,
    define: {
        timestamps: false
    }
});

connection.authenticate()
    .then(() => {
        console.log("db connected connected");
    }).catch(err => new Error(err));

module.exports = connection;

