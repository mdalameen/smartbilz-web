class InitialData {
    static _getStateCodeList() {
        return [
            { state: 'JAMMU AND KASHMIR', code: 1 },
            { state: 'HIMACHAL PRADESH', code: 2 },
            { state: 'PUNJAB', code: 3 },
            { state: 'CHANDIGARH', code: 4 },
            { state: 'UTTARAKHAND', code: 5 },
            { state: 'HARYANA', code: 6 },
            { state: 'DELHI', code: 7 },
            { state: 'RAJASTHAN', code: 8 },
            { state: 'UTTAR PRADESH', code: 9 },
            { state: 'BIHAR', code: 10 },
            { state: 'SIKKIM', code: 11 },
            { state: 'ARUNACHAL PRADESH', code: 12 },
            { state: 'NAGALAND', code: 13 },
            { state: 'MANIPUR', code: 14 },
            { state: 'MIZORAM', code: 15 },
            { state: 'TRIPURA', code: 16 },
            { state: 'MEGHLAYA', code: 17 },
            { state: 'ASSAM', code: 18 },
            { state: 'WEST BENGAL', code: 19 },
            { state: 'JHARKHAND', code: 20 },
            { state: 'ODISHA', code: 21 },
            { state: 'CHATTISGARH', code: 22 },
            { state: 'MADHYA PRADESH', code: 23 },
            { state: 'GUJARAT', code: 24 },
            { state: 'DAMAN AND DIU', code: 25 },
            { state: 'DADRA AND NAGAR HAVELI', code: 26 },
            { state: 'MAHARASHTRA', code: 27 },
            { state: 'ANDHRA PRADESH(BEFORE DIVISION)', code: 28 },
            { state: 'KARNATAKA', code: 29 },
            { state: 'GOA', code: 30 },
            { state: 'LAKSHWADEEP', code: 31 },
            { state: 'KERALA', code: 32 },
            { state: 'TAMIL NADU', code: 33 },
            { state: 'PUDUCHERRY', code: 34 },
            { state: 'ANDAMAN AND NICOBAR ISLANDS', code: 35 },
            { state: 'TELANGANA', code: 36 },
            { state: 'ANDHRA PRADESH (NEW)', code: 37 },
        ];
    }

    static getTax(dname, dpercentage, dtype) {
        return {
            name: dname,
            percentage: dpercentage,
            type: dtype
        };
    }

    static getTaxes() {
        return [
            this.getTax('GST 0%', 0.0, 'TAX_TYPE_GST_REGULAR'),
            this.getTax('GST 5%', 5.0, 'TAX_TYPE_GST_REGULAR'),
            this.getTax('GST 12%', 12.0, 'TAX_TYPE_GST_REGULAR'),
            this.getTax('GST 18%', 18.0, 'TAX_TYPE_GST_REGULAR'),
            this.getTax('GST 28%', 28.0, 'TAX_TYPE_GST_REGULAR'),
            this.getTax('COMPOSITION 1%', 1.0, 'TAX_TYPE_GST_COMPOSITION'),
            this.getTax('COMPOSITION 3%', 3.0, 'TAX_TYPE_GST_COMPOSITION'),
            this.getTax('COMPOSITION 5%', 5.0, 'TAX_TYPE_GST_COMPOSITION'),
        ];
    }

    static getUnits() {
        return [
            this.getUnit(1, 'Pieces', 'pcs', null, 1),
            this.getUnit(2, 'Meter', 'm', null, 1),
            this.getUnit(3, 'Kilometer', 'km', 2, 1000),
            this.getUnit(4, 'Centimeter', 'cm', 2, 0.01),
            this.getUnit(5, 'Millimeter', 'mm', 2, 0.001),
            this.getUnit(6, 'Inch', '\"', 2, 0.0254),
            this.getUnit(7, 'Foot', 'ft', 2, 3.281),
            this.getUnit(8, 'Yard', 'yard', 2, 0.9144),
            this.getUnit(9, 'Mile', 'mile', 2, 1609.34),
            this.getUnit(10, 'Liter', 'l', null, 1),
            this.getUnit(11, 'Milliliter', 'ml', 10, 0.001),
            this.getUnit(12, 'Kilogram', 'kg', null, 1),
            this.getUnit(13, 'Gram', 'g', 12, 0.001),
            this.getUnit(14, 'Tonne', 'ton', 12, 1000),
            this.getUnit(15, 'Milligram', 'mg', 12, 0.000001),
            this.getUnit(16, 'Pound', 'pound', 12, 0.453592),
            this.getUnit(17, 'Ounce', 'ounce', 12, 0.0283495)
        ];
    }

    static getUnit(id, name, notation, parent_id, conversion) {
        return {
            unit_id: id, unit_name: name, unit_notation: notation, parent_id: parent_id, conversion_rate: conversion
        }
    }
}
module.exports = InitialData;