const connection = require('./src/db/connection');
const DynamicDb = require('./src/db/DynamicDb');

const Client = require('./src/db/model/Client');
const InvoiceItem = require('./src/db/model/InvoiceItem');

async function createTables() {
    const c = new Client();
    let result = await c.getAllClients();
    for (let client of result) {
        console.log(client.db_name);
        let db = new DynamicDb(client.db_name);
        // // console.log(db);
        // // let result = await db.getConnection().query('select * from "sale_invoice_items;');
        // let result = await db.getConnection().query('ALTER TABLE "sale_invoice_items" ADD COLUMN "unit_id" integer;');
        let mInvoiceItem = new InvoiceItem(db);
        let result = await mInvoiceItem.getSchema().findAll();
        console.log(result);

        db.close();
    }
    console.log(result);
    connection.close();
}

createTables();