require('express-async-errors');

const express = require('express'),
    config = require('config'),
    logger = require('./src/startup/logger');

const app = express();
require('./src/startup/route')(app);


// throw new Error('new custom error arised');
const port = config.get('PORT');
app.listen(port, ()=>{
    logger.info(`server started on port ${port}`);
});