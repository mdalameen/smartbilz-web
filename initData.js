const connection = require('./src/db/connection');

const Client = require('./src/db/model/Client');

async function createTables(){
    const c = new Client();
    await c.getSchema().sync({force:true});
    console.log('client table created');
    connection.close();
}

createTables();